/**
 * 头部导航
 */
Vue.component('mk-header', {
    template: `
    <header class="mk-header" :class="{'mk-top': headerTop}">
        <div class="header-topbar">
            <div class="mk-layout">
                <div class="mk-menu"><i class="el-icon-menu" @click="drawer = true"></i></div>
                <mk-search-input></mk-search-input>
                <mk-language></mk-language>
            </div>
            <el-drawer
                direction="ltr"
                size="80%"
                :visible.sync="drawer"
                :with-header="false"
                :modal="false">
                <div class="mk-menu-drawer">
                    <div class="mk-menu-logo"><img :src="theme.logo"></div>
                    <div class="mk-menu-close" @click="drawer = false"><i class="el-icon-close"></i></div>
                    <div class="mk-menu-collapse">
                        <el-collapse 
                            v-for="(vo1, io1) in headerColumn" 
                            :key="io1" 
                            class="mk-menu-leve1" 
                            accordion>
                            <el-collapse-item :class="{'no-chilader': typeof vo1.children == 'undefined'}">
                                <template slot="title"><a :href="vo1.url">{{vo1.title}}</a></template>
                                <el-collapse 
                                    v-for="(vo2, io2) in vo1.children" 
                                    :key="io2" 
                                    class="mk-menu-leve2" 
                                    accordion>
                                    <el-collapse-item :class="{'no-chilader': typeof vo2.children == 'undefined'}">
                                        <template slot="title"><span class="string">·</span><a :href="vo2.url">{{vo2.title}}</a></template>
                                        <el-collapse 
                                            v-for="(vo3, io3) in vo2.children" 
                                            :key="io3" 
                                            class="mk-menu-leve3" 
                                            accordion>
                                            <el-collapse-item class="empty">
                                                <template slot="title"><span class="string">·</span><a :href="vo3.url">{{vo3.title}}</a></template>
                                            </el-collapse-item>
                                        </el-collapse>
                                    </el-collapse-item>
                                </el-collapse>
                            </el-collapse-item>
                        </el-collapse>
                    </div>
                </div>
            </el-drawer>
        </div>
        <section class="header-menu">
            <div class="mk-layout">
                <div class="mk-logo"><img :src="theme.logo"></div>
                <mk-search-input></mk-search-input>
                <mk-language></mk-language>
                <el-menu
                    :default-active="catalog == null ? null : catalog.id"
                    mode="horizontal"
                    background-color="transparent"
                    text-color="#666"
                    active-text-color="#272e89">
                    <template v-for="(vo1, io1) in headerColumn" :key="io1">
                        <template v-if="vo1.children">
                            <el-submenu :index="vo1.id">
                                <div 
                                    class="mk-submenu-title" 
                                    slot="title" 
                                    @click="location.href = vo1.url">
                                    {{vo1.title}}
                                </div>
                                <template 
                                    v-for="(vo2, io2) in vo1.children" 
                                    :key="io2">
                                    <template v-if="vo2.children">
                                        <el-submenu :index="vo2.id">
                                            <div 
                                                class="mk-submenu-title" 
                                                slot="title" 
                                                @click="location.href = vo2.url">
                                                {{vo2.title}}
                                            </div>
                                            <el-menu-item 
                                                v-for="(vo3, io3) in vo2.children" 
                                                :index="vo3.id" 
                                                @click="location.href = vo3.url">
                                                {{vo3.title}}
                                            </el-menu-item>
                                        </el-submenu>
                                    </template>
                                    <template v-else>
                                        <el-menu-item
                                            :index="vo2.id" 
                                            @click="location.href = vo2.url">
                                            {{vo2.title}}
                                        </el-menu-item>
                                    </template>
                                </template>
                            </el-submenu>
                        </template>
                        <template v-else>
                            <el-menu-item 
                                :index="vo1.id" 
                                @click="location.href = vo1.url">
                                {{vo1.title}}
                            </el-menu-item>
                        </template>
                    </template>
                </el-menu>
            </div>
        </section>
    </header>
    `,
    data() {
        return {
            drawer: false,
            scrollTop: 0,
            headerTop: false,
            innerWidth: window.innerWidth,
        }
    },
    created() {
        this.isHeaderTop();
    },
    mounted() {
        window.onscroll = () => {
            return (() => {
                this.isHeaderTop();
            })()
        }
        window.onresize = () => {
            return (() => {
                this.innerWidth = window.innerWidth;
            })()
        }
    },
    methods: {
        isHeaderTop () {
            let scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
            this.headerTop = scrollTop > 40 && this.innerWidth > 768;
            if (routeName !== 'index') {
                this.headerTop = true;
            }
        },
    }
});

/**
 * 会员左侧导航
 */
Vue.component('mk-personal-public', {
    template: `
    <el-tabs 
        class="mk-personal-public"
        :tab-position="tabPosition" 
        :style="{height: tabHeight, width: tabwidth}"
        @tab-click="handleClick"
        v-model="active">
        <el-tab-pane v-for="(item, index) in list" :key="index" :name="item.url">
            <div class="mk-personal-public-item" slot="label">
                <i :class="item.icon" style="margin-right:8px"></i>
                <span class="title">{{item.title}}</span>
                <span v-if="typeof item.count != 'undefined' && item.count > 0" class="badge">{{item.count}}</span>
            </div>
        </el-tab-pane>
    </el-tabs>
    `,
    data() {
        return {
            active: location.href,
            tabPosition: window.innerWidth > 768 ? 'left' : 'top',
            tabwidth: window.innerWidth > 768 ? '150px' : '100%',
            tabHeight: window.innerWidth > 768 ? window.innerHeight - 200 + 'px' : '40px',
            list: [
                {title: lang('Personal Center'), url: index_url('user/index'), icon: 'el-icon-user'},
                {title: lang('Account setup'), url: index_url('user/set'), icon: 'el-icon-setting'},
            ],
        }
    },
    mounted () {
        window.onresize = () => {
            return (() => {
                this.tabwidth = window.innerWidth > 768 ? '150px' : '100%';
                this.tabPosition = window.innerWidth > 768 ? 'left' : 'top';
                this.tabHeight = window.innerWidth > 768 ? window.innerHeight - 200 + 'px' : '40px';
            })()
        }
    },
    created() {
        console.log(personalPublic);
        if (typeof personalPublic != 'undefined' && personalPublic != null) {
            this.list = this.list.concat(personalPublic);
        }
    },
    methods: {
        handleClick(e) {
            location.href = this.list[e.index]['url']; 
        },
    }
})

/**
 * 幻灯片
 */
Vue.component('mk-swiper', {
    template: `
    <div class="mk-swiper" :id="id">
        <template v-if="thumbs === false">
            <div class="gallery-top">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div 
                            class="swiper-slide" 
                            v-for="(item, index) in list" 
                            :key="index">
                            <div v-if="preview" class="item" @click="previewClick(index)">
                                <slot name="item" v-bind:item="item"></slot>
                            </div>
                            <slot v-else name="item" v-bind:item="item"></slot>
                        </div>
                    </div>
                    <div class="swiper-pagination" v-if="pagination"></div>
                </div>
                <div class="swiper-button swiper-button-next" v-if="navigation">
                    <i :class="direction === 'horizontal' ? 'el-icon-arrow-right' : 'el-icon-arrow-down'"></i>
                </div>
                <div class="swiper-button swiper-button-prev" v-if="navigation">
                    <i :class="direction === 'horizontal' ? 'el-icon-arrow-left' : 'el-icon-arrow-up'"></i>
                </div>
            </div>
            <mk-images-preview v-if="preview" :preview="previewObj"></mk-images-preview>
        </template>
        <template v-else>
            <div class="gallery-thumbs">
                <mk-pic-zoom :src="list[thumbsIndex]"></mk-pic-zoom>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div 
                            class="swiper-slide" 
                            v-for="(item, index) in list" 
                            :key="index"
                            @click="thumbsIndex = index">
                            <img :src="typeof item.cover === 'undefined' ? item : item.cover"/>
                        </div>
                    </div>
                </div>
                <div class="swiper-button swiper-button-next">
                    <i class="el-icon-arrow-right"></i>
                </div>
                <div class="swiper-button swiper-button-prev">
                    <i class="el-icon-arrow-left"></i>
                </div>
            </div>
        </template>
    </div>`,
    props: {
        list: {
            type: Array,
            default: [],
        },
        // 方向
        direction: {
            type: String,
            default: "horizontal",
        },
        // 显示分页
        pagination: {
            type: Boolean,
            default: true,
        },
        // 显示左右
        navigation: {
            type: Boolean,
            default: true,
        },
        // 间隔
        between: {
            type: Number,
            default: 0,
        },
        // 循环
        loop: {
            type: Boolean,
            default: false,
        },
        // 自动播放
        autoplay: {
            type: Boolean,
            default: false,
        },
        // 开启缩略图
        thumbs: {
            type: Boolean,
            default: false,
        },
        thumbsBetween: {
            type: Number,
            default: 10,
        },
        // 开启预览模式
        preview: {
            type: Boolean,
            default: false,
        }
    },
    data() {
        return {
            id: "mk-",
            previewObj: {
                list: this.list,
                index: 0,
                dialog: false,
            },
            thumbsIndex: 0,
        }
    },
    mounted() {
        this.init();
    },
    methods: {
        init() {
            this.id = 'mk-' + common.id(2);
            this.$nextTick(function () {
                var topCls    = '#' + this.id + ' .gallery-top';
                var thumbsCls = '#' + this.id + ' .gallery-thumbs';
                var galleryThumbs = new Swiper(thumbsCls + ' .swiper-container', {
                    spaceBetween: this.thumbsBetween,
                    slidesPerView: 'auto',
                    freeMode: true,
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,
                    observer:true,
                    observeParents:true,
                    navigation: {
                        nextEl: thumbsCls + ' .swiper-button-next',  
                        prevEl: thumbsCls + ' .swiper-button-prev'
                    },
                });
                var thumbs = this.thumbs === true ? {swiper: galleryThumbs} : {};
                var galleryTop = new Swiper(topCls + ' .swiper-container', {
                    direction: this.direction,
                    slidesPerView: 'auto',
                    spaceBetween: this.between,
                    loop: this.loop,
                    autoplay: this.autoplay,
                    observer:true,
                    observeParents:true,
                    navigation: {
                        nextEl: topCls + ' .swiper-button-next',  
                        prevEl: topCls + ' .swiper-button-prev'
                    },
                    pagination: {
                        el: topCls + ' .swiper-pagination',
                        clickable: true,
                    },
                    thumbs: thumbs
                });
            })
        },
        previewClick(index) {
            this.previewObj = Object.assign({}, this.previewObj, {index: index, dialog: true});
        },
    },
});

/**
 * 图片预览
 */
Vue.component('mk-images-preview', {
    template: `
    <el-dialog
        top="100px"
        class="mk-images-preview"
        width="80%"
        :visible.sync="preview.dialog"
        append-to-body
        center>
        <el-image :src="url">
            <div slot="error" class="image-slot">
                <img class="error-image" :src="static + '/images/error.png'"/>
            </div>
        </el-image>
        <i class="el-icon-close" @click="preview.dialog = false"></i>
        <div class="arrow left">
            <el-button type="info" icon="el-icon-arrow-left" :disabled="left" circle @click="index--"></el-button>
        </div>
        <div class="arrow right">
            <el-button type="info" icon="el-icon-arrow-right" :disabled="right" circle @click="index++"></el-button>
        </div>
        <div class="page">
            {{index + 1}} / {{preview.list.length}}
        </div>
    </el-dialog>
    `,
    props: {
        preview: {
            type: Object,
            default: {},
        },
    },
    data() {
        return {
            left: false,
            right: false,
            index: this.preview.index,
        }
    },
    computed:{
        url() {
            return this.preview['list'][this.index]['cover'];
        }
    },
    watch: {
        index(v) {
            if (v <= 0) {
                this.index = 0;
                this.left = true;
            } else {
                this.left = false;
            }
            if (v >= this.preview['list'].length - 1) {
                this.index = this.preview['list'].length - 1;
                this.right = true;
            } else {
                this.right = false;
            }
        }
    }
});

/**
 * 放大镜
 */
Vue.component('mk-pic-zoom', {
    template: `
    <div class="mk-pic-zoom">
        <div 
            class="choose" 
            ref="choose" 
            :style="{width: width + 'px', height: height + 'px'}">
            <div 
                class="content" 
                ref="content" 
                @mousemove="handleMove" 
                @mouseout="handleOut" 
                :style="{width: width + 'px', height: height + 'px'}">
                <img :src="src" />
                <div class="shadow" ref="shadow" :style="{width: shadowWidth + 'px', height: shadowHeight + 'px'}"></div>
            </div>
        </div>
        <div class="larger" ref="larger" :style="{width: width + 'px', height: height + 'px', left: largerLeft + 'px'}">
            <img
                :src="src" 
                ref="big"
                :style="{maxWidth: largerWidth + 'px', width: largerWidth + 'px', height: largerHeight + 'px'}"/>
        </div>
    </div>`,
    props: {
        src: {
            type: String,
        },
        width: {
            type: Number,
            default: 400
        },
        height: {
            type: Number,
            default: 400
        }
    },
    data() {
        return {

        }
    },
    computed: {
        shadowWidth() {
            return this.width / 2;
        },
        shadowHeight() {
            return this.height / 2;
        },
        largerWidth() {
            return this.width * 2;
        },
        largerHeight() {
            return this.height * 2;
        },
        largerLeft() {
            return this.width + 60;
        },
    },
    methods: {
        getPosition: function(element){ 
            var dc = document, 
            rec = element.getBoundingClientRect(), 
            x = rec.left, // 获取元素相对浏览器视窗window的左、上坐标 
            y = rec.top; 
            // 与html或body元素的滚动距离相加就是元素相对于文档区域document的坐标位置 
            x += dc.documentElement.scrollLeft || dc.body.scrollLeft; 
            y += dc.documentElement.scrollTop || dc.body.scrollTop;
            return {
                left: x, 
                top: y 
            }; 
        },
        handleMove(evt) {
            var larger = this.$refs.larger;
            var shadow = this.$refs.shadow;
            var big = this.$refs.big;
            var pos = this.getPosition(this.$refs.choose);
            var shadowRect = shadow.getBoundingClientRect();
            var bigRect = big.getBoundingClientRect();
            var contentRect = this.$refs.content.getBoundingClientRect();
            var maxX = contentRect.width - shadowRect.width;
            var maxY = contentRect.height - shadowRect.height;
 
            var X = evt.pageX - pos.left - shadowRect.width / 2;
            var Y = evt.pageY - pos.top - shadowRect.height / 2;
 
            if (X <= 0) {
                X = 0;
            }
            if (X >= maxX) {
                X = maxX;
            }
            if (Y <= 0) {
                Y = 0;
            }
            if (Y >= maxY) {
                Y = maxY;
            }
            // 防止遮罩层粘滞，跟随鼠标一起滑出大图位置
            var bigX = X * bigRect.width / contentRect.width;
            var bigY = Y * bigRect.height / contentRect.height;
            //  bigX / bigW = X / contentW,主图和遮罩层之间存在两倍关系，放大图和原图之间也有两倍关系
            shadow.style.left = X + "px";
            shadow.style.top = Y + "px";
            // console.log(X, Y, bigX, bigY);
            big.style.left = -bigX + "px";
            big.style.top = -bigY + "px";
            larger.style.display = "block";
            shadow.style.display = "block";
            shadow.style.opacity = 1;
        },
        handleOut(evt) {
            var larger = this.$refs.larger;
            var shadow = this.$refs.shadow;
            larger.style.display = "none";
            shadow.style.opacity = 0;
            shadow.style.top = "-1000px";
        }
    }
});

/**
 * 全站搜索
 */
Vue.component('mk-search-input', {
    template: `
    <div class="mk-search-input">
        <el-popover
            placement="bottom"
            width="300"
            trigger="click">
            <el-input
                :placeholder="lang('search') + '...'"
                v-model="keyword"
                @keyup.enter.native="location()">
                <i slot="suffix" class="el-input__icon el-icon-search" @click="location()"></i>
            </el-input>
            <div class="mk-search-button" slot="reference"><i class="el-icon-search"></i></div>
        </el-popover>
    </div>
    `,
    props: {
        color: {
            type: String,
            default: '#fff',
        }
    },
    data() {
        return {
            search: false,
            keyword: "",
            modular: "",
        }
    },
    methods: {
        location() {
            if (this.keyword != '') {
                location.href = index_url('search/' + this.keyword);
            }
        },
    }
});

/**
 * 个人信息
 */
Vue.component('mk-userinfo', {
    template: `
    <template v-if="userInfo === null">
        <a class="mk-userinfo" :href="index_url('user/login')">{{lang("Please log in")}}</a>
    </template>
    <template v-else>
        <el-dropdown
            @command="clickInfo"
            @visible-change="show = show ? false :true"
            :class="{'flag-show': show}"
            class="mk-userinfo">
            <div class="mk-userinfo-selected">
                <el-avatar :size="30" :src="domain + userInfo.cover">
                    {{userInfo.cover === "" ? userInfo.nickname.substr(0,1) : ''}}
                </el-avatar>
                <span class="mk-icon-down el-icon-arrow-down"></span>
            </div>
            <el-dropdown-menu slot="dropdown">
                <el-dropdown-item
                v-for="(item, index) in list"
                :key="index"
                :command="item">
                    <el-dropdown-item class="mk-userinfo-list-icon" :icon="item.icon">{{lang(item.title)}}</el-dropdown-item>
                </el-dropdown-item>
            </el-dropdown-menu>
        </el-dropdown>
    </template>
    `,
    data: function () {
        return {
            show: false,
            list: [
                {title: "Personal Center", icon: "el-icon-house", value: "user/index"},
                {title: "Log out", icon: "el-icon-switch-button", value: "user/logout"},
            ],
        }
    },
    methods: {
        /**
         * 选择项
         * @param  {Object} item
         */
        clickInfo(item) {
            var self = this;
            location.href = index_url(item.value);
        },
    }
});

/**
 * 语言切换
 */
Vue.component('mk-language', {
    template: `
    <el-dropdown
        @command="clickLang"
        @visible-change="flagShow = flagShow ? false :true"
        class="mk-language"
        :class="{'flag-show': flagShow}">
        <div class="mk-language-selected">
            <el-image class="mk-language-flag-icon" :src="cover" style="float:left">
                <div slot="error" class="image-slot">
                    <img class="error-image" :src="static + '/images/error.png'"/>
                </div>
            </el-image>
            <span class="mk-language-title">{{title}}</span>
            <span class="mk-icon-down el-icon-arrow-down"></span>
        </div>
        <el-dropdown-menu slot="dropdown">
            <el-dropdown-item
            v-for="(item, index) in langAllow"
            :key="index"
            :command="item">
                <div class="mk-language-flag-warp">
                    <el-image class="mk-language-flag-icon" :src="item.cover">
                        <div slot="error" class="image-slot">
                            <img class="error-image" :src="static + '/images/error.png'"/>
                        </div>
                    </el-image>
                    {{lang(item.name)}}
                </div>
            </el-dropdown-item>
        </el-dropdown-menu>
    </el-dropdown>
    `,
    data: function () {
        return {
            flagShow: false,
        }
    },
    computed: {
        cover() {
            let index = common.arrayIndex(langAllow, language, 'name');
            return index === -1 ? "" : langAllow[index]['cover'];
        },
        title() {
            let index = common.arrayIndex(langAllow, language, 'name');
            return index === -1 ? "" : langAllow[index]['title'];
        }
    },
    methods: {
        /**
         * 选择语言
         * @param  {Object} item 语言项
         */
        clickLang(item) {
            location.href=item.url;
        },
    }
});

/**
 * 返回顶部
 */
Vue.component('mk-gotop', {
    template: `
    <div 
        v-show="gotop" 
        class="mk-gotop" 
        @click="clickGotop()">
        <i class="el-icon-caret-top"></i>
    </div>`,
    data() {
        return {
            gotop: false,
            top: 0,
        }
    },
    created(){
        document.addEventListener('scroll', this.handleScroll)
    },
    methods: {
        /**
         * 显示返回顶部按钮
         */
        handleScroll() {
            let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop // 滚动条偏移量
            this.gotop    = scrollTop > 200;
        },
        /**
         * 返回顶部
         */
        clickGotop() {
            $("html, body").animate({ scrollTop:0 }, 1000);
        },
    },
    beforeDestroy () {
        document.removeEventListener('scroll', this.handleScroll);
    },
});

/**
 * 登录表单
 */
Vue.component('mk-login-form', {
    template: `
    <el-form 
        class="mk-login-form" 
        :model="loginForm" 
        :rules="rules" 
        ref="loginForm"
        @submit.native.prevent>
        <template v-if="captchaShow">
            <div class="mk-captcha-title">{{lang('Machine verification')}}</div>
            <el-form-item v-if=" captcha !== '' " prop="captcha">
                <img 
                    class="mk-captcha-img" 
                    :src="captcha" 
                    @click="getCaptcha()" />
                <el-input 
                    class="mk-captcha-code" 
                    v-model="loginForm.captcha" 
                    :placeholder="lang('Please enter the graphic verification code above')"
                    @keyup.enter.native="submitForm()">
                </el-input>
            </el-form-item>
            <el-button
                :disabled="loginForm.captcha.length === 4 ? false : true"
                class="login-btn" 
                @click="submitForm()" 
                :loading="loading" 
                plain>
                {{lang('verify')}}
            </el-button>
        </template>
        <template v-else>
            <el-form-item prop="loginAccount">
                <el-input 
                    v-model="loginForm.loginAccount" 
                    prefix-icon="el-icon-user" 
                    :placeholder="lang('Please enter account/email number')"
                    @keyup.enter.native="getCaptcha()">
                </el-input>
            </el-form-item>
            <el-form-item prop="loginPassword">
                <el-input 
                    v-model="loginForm.loginPassword" 
                    prefix-icon="el-icon-key" 
                    :placeholder="lang('Please enter the password')" 
                    show-password
                    @keyup.enter.native="getCaptcha()">
                </el-input>
            </el-form-item>
            <el-form-item prop="checked">
                <el-checkbox v-model="loginForm.checked">{{lang('Log in automatically within two weeks')}}</el-checkbox>
            </el-form-item>
            <el-button 
                class="login-btn" 
                @click="getCaptcha()" 
                :loading="loading"
                plain>
                登录
            </el-button>
            <div class="regOrPwd">
                <a :href="index_url('user/password')" class="password">{{lang('Already have an account, forget your password?')}}</a>
                <a :href="index_url('user/register')" class="register">{{lang('Sign up now')}}</a>
            </div>
            <div class="login-footer">
                <div class="login-footer-item"><el-divider>{{lang('worth mentioning')}}</el-divider></div>
            </div>
            <a class="login-api-item" v-for="(item, index) in loginApi" :key="index" :href="item.url" :title="item.title"><img :src="item.cover"></a>
        </template>
    </el-form>`,
    data() {
        return {
            loading: false,
            storageTime: 240*60*60*1000,
            captcha: "",
            captchaShow: false,
            loginForm: {
                loginAccount: "",
                loginPassword: "",
                captcha: "",
                checked: false,
            },
            rules: {
                loginAccount: [
                    { required: true, message: lang('Please input Username'), trigger: 'blur' },
                ],
                loginPassword: [
                    { required: true, message: lang('Please enter the password'), trigger: 'blur' },
                ],  
                captcha: [
                    { min: 4, message: lang('The length of the verification code is incorrect'), trigger: 'blur' }
                ],  
            },
        }
    },
    methods: {
        /**
         * 准备登录
         */
        getCaptcha() {
            let self = this;
            request.post('user/isNeedVerification', {}, function(res){
                let url = index_url('verify');
                let str = url.indexOf('?') === -1 ? '?' : '&';
                self.captcha = res === 1 ? url + str + Math.random() : '';
                self.$refs.loginForm.validate((valid) => {
                    if (valid) {
                        if (self.captcha !== '') {
                            self.captchaShow  = true;
                        } else {
                            self.submitForm();
                        }
                    } else {
                        return false;
                    }
                });
            })
        },
        /**
         * 登录
         */
        submitForm() {
            let self = this;
            self.loading = true;
            request.post('user/login', self.loginForm, function (res){
                self.loading = false;
                self.$message({ showClose: true, message: res.message, type: res.status});
                if(res.status === 'success'){
                    self.$emit('success-login', res.url);
                } else {
                    self.captchaShow       = false;
                    self.loginForm.captcha = "";
                }
            })
        },
    },
});

/**
 * 登录弹窗
 */
Vue.component('mk-login-dialog', {
    template: `
    <el-dialog
        top="90px"
        :title="lang('User login')"
        width="360px"
        class="mk-login-dialog"
        :visible.sync="loginDialog"
        :close-on-click-modal="false"
        center>
        <div class="mk-aside-logo-warp"><div></div></div>
        <mk-login-form @success-login="success($event)"></mk-login-form>
    </el-dialog>`,
    data() {
        return store.state
    },
    methods: {
        success(res) {
            this.loginDialog = false;
        }
    },
});

/**
 * 编辑器默认是textarea
 */
Vue.component('mk-editor', {
    template: `
        <div class="mk-editor">
            <el-input type="textarea" v-model="content" :rows="10"></el-input>
        </div>
    `,
    props: {
        value: {
            type: String,
            default: "",
        },
        height: {
            type: Number,
            default: 400,
        },
    },
    data() {
        return {
            content: this.value,
        }
    },
    watch: {
        value(v) {
            this.content = v;
        },
        content(v) {
            this.value = v;
            this.$emit('input', v);
        },
    }
})

/**
 * 上传
 */
Vue.component('mk-upload', {
    template: `
        <el-upload
            class="file-upload"
            :show-file-list="false"
            :action="index_url(uploadUrl)"
            :before-upload="beforeUpload"
            :on-success="successUpload"
            :on-error="errorUpload">
            <slot></slot>
        </el-upload>
    `,
    data() {
        return {
            url: "",
            uploadUrl: "user/upload",
        }
    },
    methods: {
        /**
         * 准备上传
         */
        beforeUpload(item) {

        },
        /**
         * 上传成功回调
         * @param  {Object} res   返回当前状态
         * @param  {Object} item  当前文件
         */
        successUpload(res, item) {
            var self = this;
            if (res.status === 'success') {
                self.$emit('success', res.data);
            } else {
                if (res.status === 'login') store.setLoginDialog(true); // 打开登录弹窗
                self.$message({showClose: true, message: res.message, type: res.status});
            }
        },
        /**
         * 上传错误回调
         * @param  {Object} err 错误信息
         */
        errorUpload(err) {
            this.$message({showClose: true, message: lang('The server is not responding'), type: 'error'});
        },
    }
})