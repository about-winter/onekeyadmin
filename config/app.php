<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------
return [
    // 应用地址
    'app_host'                => 'app',
    // 应用的命名空间
    'app_namespace'           => '',
    // 是否启用路由
    'with_route'              => true,
    // 是否启用事件
    'with_event'              => true,
    // 开启应用快速访问
    'app_express'             => true,
    // 默认应用
    'default_app'             => 'index',
    // 默认时区
    'default_timezone'        => 'Asia/Shanghai',
    // 应用映射（自动多应用模式有效）
    'app_map'                 => [env('map_admin', 'mk_admin') => 'admin'],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'             => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'           => ['addons'],
    // 异常页面的模板文件
    'exception_tmpl'          => app()->getThinkPath() . 'tpl/think_exception.tpl',
    // 错误显示信息,非调试模式有效
    'error_message'           => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'          => true,
    // 错误页面
    'http_exception_template' =>  [],
    // 请求地址
    'api' => 'http://www.onekeyadmin.com',
    // 版本号
    'version' => '1.2.4',
    // 电脑模板
    'pc_template' => 'template',
    // 手机模板
    'mp_template' => 'template',
    // 语言列表
    'language' => 'a:2:{i:0;a:6:{s:5:"cover";s:59:"/upload/image/20210306/a84150e1f663fead3046086a17a3e72a.png";s:5:"title";s:6:"中文";s:4:"name";s:2:"cn";s:7:"default";i:1;s:6:"status";i:1;s:4:"sort";i:2;}i:1;a:6:{s:5:"cover";s:59:"/upload/image/20210306/8bec5969b29bee5d3994e3e19b5bb757.png";s:5:"title";s:6:"英语";s:4:"name";s:2:"en";s:7:"default";i:0;s:4:"sort";i:1;s:6:"status";i:1;}}',
];
