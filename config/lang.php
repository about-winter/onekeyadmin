<?php
// +----------------------------------------------------------------------
// | 多语言设置
// +----------------------------------------------------------------------
$info = lang_config();
$extend_list = [];
foreach ($info['allow'] as $key => $val) {
    $array[] = public_path() . 'lang/' . $val['name'] . ".php";
    foreach (plugin_list() as $key => $plugins) {
        $file = plugin_path() . $plugins['name'] . '/lang/' .  $val['name'] . ".php";
        if (is_file($file)) {
            $array[] = $file;
        }
    }
    $extend_list[$val['name']] = $array;
}
return [
    // 允许语言
    'lang_allow'   => $info['allow'],
    // 默认语言
    'default_lang' => $info['default'],
    // 多语言自动侦测变量名
    'detect_var'   => 'lang',
    // 是否使用Cookie记录
    'use_cookie'   => false,
    // Cookie名称
    'cookie_var'   => 'lang',
    // 扩展语言包
    'extend_list'  => $extend_list,
    // 是否支持语言分组
    'allow_group'  => true,
];