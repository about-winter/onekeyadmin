<?php

// +----------------------------------------------------------------------
// | 日志设置
// +----------------------------------------------------------------------
return [
    // 默认日志记录通道
    'default'      => 'system',
    // 日志记录级别
    'level'        => [],
    // 日志类型记录的通道 
    'type_channel' => [
        'system'   => 'system',
        'business' => 'business',
    ],
    // 关闭全局日志写入
    'close'        => false,
    // 全局日志处理 支持闭包
    'processor'    => null,
    // 日志通道列表
    'channels'     => [
        // 全局应用系统日志
        'system' => [
            // 日志记录方式
            'type'           => 'File',
            // 单文件日志写入
            'single'         => 'system',
            // 独立日志级别
            'apart_level'    => [],
            // 日志保存目录
            'path' => App()->getRuntimePath() . 'log',
            // 最大日志文件数量（为空时无限备份，为1时其它通道无效，为2时主管道会备份“一次”）
            'max_files'      => 2,
            // 文件大小限制
            'file_size'      => 1024*1024*25,
            // 使用JSON格式记录
            'json'           => true,
            // 日志处理
            'processor'      => null,
            // 关闭通道日志写入
            'close'          => false,
            // 日志记录时间格式
            'time_format'    => 'Y-m-d H:i:s',
            // 日志输出格式化
            'format'         => '[%s][%s]:%s',
            // 是否实时写入
            'realtime_write' => false,
        ],
        // admin应用业务日志
        'business' => [
            // 日志记录方式
            'type'           => 'File',
            // 单文件日志写入
            'single'         => 'business',
            // 独立日志级别
            'apart_level'    => [],
            // 日志保存目录
            'path' => App()->getRuntimePath() . 'admin/log',
            // 最大日志文件数量
            'max_files'      => 2,
            // 文件大小限制
            'file_size'      => 1024*1024*25,
            // 使用JSON格式记录
            'json'           => true,
            // 日志处理
            'processor'      => null,
            // 关闭通道日志写入
            'close'          => false,
            // 日志记录时间格式
            'time_format'    => 'Y-m-d H:i:s',
            // 日志输出格式化
            'format'         => '[%s][%s]:%s',
            // 是否实时写入
            'realtime_write' => false,
        ],
    ],
];
