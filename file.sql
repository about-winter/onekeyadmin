/*
SQLyog Ultimate v12.3.1 (64 bit)
MySQL - 5.7.26 : Database - onekeyadmin
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*Table structure for table `mk_admin_group` */

CREATE TABLE IF NOT EXISTS `mk_admin_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `role` text NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0屏蔽 1正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `mk_admin_group` */

insert  into `mk_admin_group`(`id`,`pid`,`title`,`role`,`status`) values (1,0,'超级管理员','*',1);
/*Table structure for table `mk_admin_menu` */

CREATE TABLE IF NOT EXISTS `mk_admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `sort` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0屏蔽 1正常',
  `ifshow` tinyint(1) NOT NULL COMMENT '左侧菜单是否显示',
  `logwriting` tinyint(1) NOT NULL COMMENT '1写入日志 0不写入',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8;

/*Data for the table `mk_admin_menu` */

insert  into `mk_admin_menu`(`id`,`pid`,`title`,`icon`,`path`,`sort`,`status`,`ifshow`,`logwriting`) values 
(1,0,'控制台','el-icon-s-home','console/index',9,1,1,0),
(2,0,'常规管理','el-icon-s-tools','config',7,1,1,0),
(3,2,'个人中心','','adminUser/personal',1,1,1,0),
(4,2,'系统配置','','config/index',4,1,1,0),
(5,0,'资源库','el-icon-upload','file/index',8,1,1,0),
(6,2,'分类管理','','catalog/index',3,1,1,0),
(7,0,'权限管理','el-icon-s-custom','authority',6,1,1,0),
(8,7,'管理员管理','','adminUser/index',4,1,1,0),
(9,7,'角色组','','adminGroup/index',0,1,1,0),
(11,0,'会员管理','el-icon-user-solid','user',5,1,1,0),
(12,11,'会员管理','','user/index',0,1,1,0),
(13,11,'会员分组','','userGroup/index',0,1,1,0),
(15,0,'插件商店','el-icon-s-promotion','plugins/list',4,1,1,0),
(17,15,'查看','','plugins/index',0,1,0,0),
(18,7,'管理员日志','','adminUser/log',3,1,1,0),
(19,7,'菜单规则','','adminMenu/index',2,1,1,0),
(113,9,'查看','','adminGroup/index',0,1,0,0),
(114,9,'删除','','adminGroup/delete',0,1,0,0),
(115,9,'编辑','','adminGroup/update',0,1,0,0),
(118,19,'查看','','adminMenu/index',0,1,0,0),
(119,19,'删除','','adminMenu/delete',0,1,0,0),
(120,19,'修改','','adminMenu/update',0,1,0,0),
(121,9,'添加','','adminGroup/save',0,1,0,0),
(122,19,'添加','','adminMenu/save',0,1,0,0),
(123,18,'查看','','adminUser/log',0,1,0,0),
(124,18,'删除','','adminUser/logdelete',0,1,0,0),
(125,8,'查看','','adminUser/index',0,1,0,0),
(126,8,'添加','','adminUser/save',0,1,0,0),
(127,8,'删除','','adminUser/delete',0,1,0,0),
(128,8,'修改','','adminUser/update',0,1,0,0),
(132,6,'查看','','catalog/index',0,1,0,0),
(133,6,'删除','','catalog/delete',0,1,0,0),
(134,6,'编辑','','catalog/update',0,1,0,0),
(135,6,'添加','','catalog/save',0,1,0,0),
(137,12,'添加','','user/save',0,1,0,0),
(138,12,'删除','','user/delete',0,1,0,0),
(139,12,'编辑','','user/update',0,1,0,0),
(141,13,'查看','','userGroup/index',0,1,0,0),
(142,13,'删除','','userGroup/delete',0,1,0,0),
(143,13,'添加','','userGroup/save',0,1,0,0),
(144,13,'编辑','','userGroup/update',0,1,0,0),
(145,12,'查看','','user/index',0,1,0,0),
(147,4,'链接','','config/link',0,1,0,0),
(148,4,'编辑','','config/update',0,1,0,0),
(150,2,'语言管理','','lang/index',2,1,1,0),
(151,150,'查看','','lang/index',0,1,0,1),
(152,150,'编辑','','lang/update',0,1,0,1),
(154,5,'上传','','file/upload',0,1,0,1),
(155,3,'修改','','adminUser/personalUpdate',0,1,0,0),
(156,5,'新建文件夹','','file/addFolder',0,1,0,0),
(157,5,'列表','','file/index',0,1,0,0),
(158,5,'删除','','file/delete',0,1,0,0),
(160,5,'修改','','file/update',0,1,0,0),
(161,5,'回收站','','file/recycle',0,1,0,0),
(162,5,'粘贴','','file/paste',0,1,0,0),
(164,5,'水印编辑','','file/watermarkUpdate',0,1,0,0),
(165,5,'计算','','file/countSizeFolder',0,1,0,0),
(168,4,'查看','','config/index',0,1,0,0),
(182,6,'同步','','catalog/synchro',0,1,0,0),
(183,6,'设置','','catalog/set',0,1,0,0),
(188,15,'卸载','','plugins/delete',0,1,0,1),
(189,15,'详情','','plugins/details',0,1,0,1),
(190,15,'修改','','plugins/update',0,1,0,1),
(191,0,'主题商店','el-icon-star-on','themes/index',3,1,1,0),
(192,191,'主题','','themes/index',0,1,0,0),
(193,191,'卸载','','themes/delete',0,1,0,0),
(194,191,'启动','','themes/update',0,1,0,0),
(195,191,'安装','','themes/install',0,1,0,0),
(196,191,'配置','','themes/config',0,1,0,0),
(197,191,'更改配置','','themes/configUpdate',0,1,0,0),
(198,15,'评论','','plugins/comment',0,1,0,1),
(199,15,'安装','','plugins/install',0,1,0,1),
(200,2,'轮播管理','','banner/index',3,1,1,0),
(201,200,'列表','','banner/index',3,1,0,0),
(202,200,'添加','','banner/save',3,1,0,0),
(203,200,'删除','','banner/delete',3,1,0,0),
(204,200,'修改','','banner/update',3,1,0,0),
(205,5,'上传指定文件','','file/uploadAppoint',0,1,0,1),
(206,1,'清除缓存','','index/cacheClear',0,1,0,1),
(207,1,'检测更新','','index/checkUpdate',0,1,0,1),
(208,1,'系统更新','','index/update',0,1,0,1),
(209,200,'同步','','banner/synchro',0,1,0,0),
(210,6,'拖动排序','','catalog/sort',0,1,0,0),
(211,200,'拖动排序','','banner/sort',0,1,0,0),
(212,15,'购买','','plugins/createOrder',0,1,0,1),
(213,191,'购买','','themes/createOrder',0,1,0,0),
(214,191,'上传','','themes/upload',0,1,0,0),
(215,15,'上传','','plugins/upload',0,1,0,1),
(216,6,'复制到其它语言','','catalog/copyList',0,1,0,0),
(217,6,'语言分类','','catalog/copyListCatalog',0,1,0,0),
(218,200,'复制到其它语言','','banner/copyList',3,1,0,0),
(219,150,'删除','','lang/delete',0,1,0,1),
(220,150,'排序','','lang/sort',0,1,0,1);
/*Table structure for table `mk_admin_user` */

CREATE TABLE IF NOT EXISTS `mk_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `nickname` varchar(255) NOT NULL COMMENT '昵称',
  `sex` tinyint(1) NOT NULL COMMENT '0男1女',
  `email` varchar(255) NOT NULL COMMENT '邮箱号',
  `account` varchar(255) NOT NULL COMMENT '登录账号',
  `password` varchar(60) NOT NULL COMMENT '登录密码',
  `cover` varchar(255) NOT NULL COMMENT '头像',
  `login_ip` varchar(15) NOT NULL,
  `login_count` int(11) NOT NULL,
  `login_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `create_time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0屏蔽 1正常',
  `field` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `mk_admin_user` */

/*Table structure for table `mk_banner` */

CREATE TABLE IF NOT EXISTS `mk_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catalog_id` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `link` text NOT NULL,
  `sort` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `language` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `mk_banner` */

insert  into `mk_banner`(`id`,`catalog_id`,`cover`,`video`,`title`,`detail`,`content`,`link`,`sort`,`status`,`language`,`create_time`) values 

(1,'232,233,234,235','/upload/image/20210811/b6e3e3158ed1115cf56f5342dbe41d58.jpg','','','','','',1,1,'cn','2021-08-11 00:56:04'),

(2,'232,233,234,235','/upload/image/20210811/7fc9db45a72e017b6b628a77133baeb2.jpg','','','','','',2,1,'cn','2021-08-11 00:56:12');

/*Table structure for table `mk_catalog` */

CREATE TABLE IF NOT EXISTS `mk_catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `num` int(11) NOT NULL COMMENT '栏目标识,区分多语言',
  `group_id` varchar(255) NOT NULL COMMENT '用户权限',
  `title` varchar(255) NOT NULL COMMENT '名称',
  `cover` varchar(255) NOT NULL COMMENT '封面',
  `content` text NOT NULL COMMENT '内容',
  `field` text NOT NULL COMMENT '自定义字段',
  `seo_url` varchar(255) NOT NULL COMMENT '目录链接(路由)',
  `seo_title` varchar(255) NOT NULL COMMENT '页面标题',
  `seo_keywords` varchar(255) NOT NULL COMMENT '页面关键字',
  `seo_description` varchar(255) NOT NULL COMMENT '页面描述',
  `links_type` tinyint(1) NOT NULL COMMENT '1自定义链接',
  `links_value` text NOT NULL COMMENT '连接页面',
  `sort` int(11) NOT NULL COMMENT '排序',
  `type` varchar(255) NOT NULL,
  `show` tinyint(1) NOT NULL COMMENT '0不显示1都显示2头部显示3底部显示',
  `status` tinyint(1) NOT NULL COMMENT '0屏蔽 1正常',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `language` varchar(255) NOT NULL COMMENT '语言',
  `mobile` tinyint(1) NOT NULL COMMENT '手机栏目',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;

/*Data for the table `mk_catalog` */

insert  into `mk_catalog`(`id`,`pid`,`num`,`group_id`,`title`,`cover`,`content`,`field`,`seo_url`,`seo_title`,`seo_keywords`,`seo_description`,`links_type`,`links_value`,`sort`,`type`,`show`,`status`,`create_time`,`language`,`mobile`) values 

(232,0,0,'','主页','','','[]','index','','','',0,'null',4,'page',1,1,'2021-05-24 11:47:10','cn',1),

(233,0,0,'','产品中心','','','[]','product','','','',0,'null',3,'product',1,1,'2021-05-24 11:47:26','cn',1),

(234,0,0,'','新闻中心','','','[]','news','','','',0,'null',2,'news',1,1,'2021-05-24 11:47:35','cn',1),

(235,0,0,'','联系我们','','','[]','contact-us','','','',0,'null',1,'page',1,1,'2021-05-25 10:17:02','cn',1),

(236,0,0,'','Home','','','[]','index','','','',0,'null',0,'page',1,1,'2021-05-24 11:47:10','en',1),

(237,0,0,'','Products','','','[]','product','','','',0,'null',0,'product',1,1,'2021-05-24 11:47:26','en',1),

(238,0,0,'','News','','','[]','news','','','',0,'null',0,'news',1,1,'2021-05-24 11:47:35','en',1),

(239,0,10252,'','Daily News','','','[]','daily_news','Daily News','','In addition to high performance products, Liebherr provides reliable global service. That means we allow you to work safely and efficiently – even in extreme conditions.',0,'null',0,'news',1,1,'2021-05-24 14:39:02','en',1),

(240,0,10251,'','Small crawler','','','[]','small_crawler_crane','','','Our range of Maeda Mini Crawler Cranes have been designed to carry out lifting works even in the most difficult areas to access.',0,'null',0,'product',1,1,'2021-05-24 16:58:25','en',1),

(241,0,0,'','Contact us','','','[]','contact-us','','','',0,'null',0,'page',1,1,'2021-05-25 10:17:02','en',1),

(242,0,0,'','电子画册','','','[]','flipbook','','','',0,'null',0,'flipbook',1,1,'2021-08-05 11:40:48','en',1),

(243,0,10252,'','Daily News','','','[]','daily_news','Daily News','','In addition to high performance products, Liebherr provides reliable global service. That means we allow you to work safely and efficiently – even in extreme conditions.',0,'null',0,'news',1,1,'2021-05-24 14:39:02','en',1),

(244,0,10251,'','Small crawler','','','[]','small_crawler_crane','','','Our range of Maeda Mini Crawler Cranes have been designed to carry out lifting works even in the most difficult areas to access.',0,'null',0,'product',1,1,'2021-05-24 16:58:25','en',1);

/*Table structure for table `mk_config` */

CREATE TABLE IF NOT EXISTS `mk_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '名称',
  `name` varchar(255) NOT NULL COMMENT '别名',
  `value` text NOT NULL COMMENT '值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `mk_config` */

insert  into `mk_config`(`id`,`title`,`name`,`value`) values 

(1,'基础配置','basics_cn','{\"email\":\"513038XXX@qq.com\",\"telephone\":\"15757396XXX\",\"address\":\"\\u6d59\\u6c5f\\u7701\\u6e29\\u5dde\\u5e02\",\"copyright\":\"\\u00a9 Copyright - 2020-2030 : All Rights Reserved.\",\"seo_title\":\"OneKeyAdmin-\\u57fa\\u4e8eThinkPHP\\u548cElement\\u7684\\u540e\\u53f0\\u6846\\u67b6\",\"seo_keywords\":\"OneKeyAdmin,ThinkPHP6\\u901a\\u7528\\u540e\\u53f0,ThinkPHP\\u6846\\u67b6,Element\\u540e\\u53f0,PHP\\u63d2\\u4ef6\\u5e94\\u7528\\u5e02\\u573a\",\"seo_description\":\"OneKeyAdmin\\u662f\\u4e00\\u6b3e\\u57fa\\u4e8eThinkPHP\\u548cElement\\u7684\\u4e00\\u952e\\u5f0f\\u540e\\u53f0\\u5f00\\u53d1\\u6846\\u67b6\",\"icon\":\"\\/upload\\/image\\/20210306\\/b9bfcd880f322c03dac531c6892f0c72.png\",\"logo\":\"\\/upload\\/image\\/20210429\\/365d30710c0fbd9ca0dcc0fec5b05575.png\",\"ico\":\"\\/upload\\/favicon.ico?3200679\",\"business_hours\":\"\",\"fax\":\"15757396XXX\"}'),

(2,'基础配置','basics_en','{\"email\":\"513038XXX@qq.com\",\"telephone\":\"15757396XXX\",\"address\":\"\\u6d59\\u6c5f\\u7701\\u6e29\\u5dde\\u5e02\",\"copyright\":\"\\u00a9 Copyright - 2020-2030 : All Rights Reserved.\",\"seo_title\":\"OneKeyAdmin-\\u57fa\\u4e8eThinkPHP\\u548cElement\\u7684\\u540e\\u53f0\\u6846\\u67b6\",\"seo_keywords\":\"OneKeyAdmin,ThinkPHP6\\u901a\\u7528\\u540e\\u53f0,ThinkPHP\\u6846\\u67b6,Element\\u540e\\u53f0,PHP\\u63d2\\u4ef6\\u5e94\\u7528\\u5e02\\u573a\",\"seo_description\":\"OneKeyAdmin\\u662f\\u4e00\\u6b3e\\u57fa\\u4e8eThinkPHP\\u548cElement\\u7684\\u4e00\\u952e\\u5f0f\\u540e\\u53f0\\u5f00\\u53d1\\u6846\\u67b6\",\"icon\":\"\\/upload\\/image\\/20210306\\/b9bfcd880f322c03dac531c6892f0c72.png\",\"logo\":\"\\/upload\\/image\\/20210429\\/365d30710c0fbd9ca0dcc0fec5b05575.png\",\"ico\":\"\\/upload\\/favicon.ico?3200679\",\"business_hours\":\"\",\"fax\":\"15757396XXX\"}'),

(3,'邮箱配置','email','{\"email\":\"513038996@qq.com\",\"password\":\"\",\"sender\":\"onekeyadmin\",\"smtp\":\"smtp.qq.com\",\"sendstyle\":\"ssl\"}'),

(4,'用户配置','user','{\"status\":\"1\",\"signin\":\"1\",\"invite\":\"2\"}'),

(5,'默认主题配置','theme_template_cn','{\"logo\":\"\\/upload\\/image\\/20210524\\/3f78a16cb20bcc3db76ce0d06991ef7e.png\",\"background_image\":\"\\/upload\\/image\\/20210524\\/279c731486916a91380237ab4214bfc1.jpg\",\"index_about_banner\":[{\"id\":63,\"pid\":62,\"title\":\"about-banner-1.jpg\",\"url\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"size\":70971,\"type\":\"image\",\"folder\":0,\"create_time\":\"2021-05-24 11:44:07\",\"status\":1,\"typeName\":\"\\u56fe\\u7247\",\"sizeName\":\"69.31KB\",\"cover\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"rename\":true,\"shear\":false,\"search\":{\"pid\":0,\"page\":1,\"totalPage\":1,\"isLoad\":false,\"noMore\":false,\"keyword\":\"\"},\"countSizeLoading\":false},{\"id\":63,\"pid\":62,\"title\":\"about-banner-1.jpg\",\"url\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"size\":70971,\"type\":\"image\",\"folder\":0,\"create_time\":\"2021-05-24 11:44:07\",\"status\":1,\"typeName\":\"\\u56fe\\u7247\",\"sizeName\":\"69.31KB\",\"cover\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"rename\":true,\"shear\":false,\"search\":{\"pid\":0,\"page\":1,\"totalPage\":1,\"isLoad\":false,\"noMore\":false,\"keyword\":\"\"},\"countSizeLoading\":false}],\"index_about_title\":\"XX\\u54c1\\u724c\\u4ea7\\u54c1\",\"index_about_content\":\"\",\"index_about_url\":{\"type\":\"3\",\"title\":\"#\",\"targer\":\"0\",\"value\":[{\"value\":\"\"},{\"value\":\"\"},{\"value\":\"\",\"linkName\":\"news\"},{\"value\":\"#\",\"pageId\":\"181\"}]},\"index_about_desc\":\"\\u4e2d\\u56fd\\u7684\\u6280\\u672f\\u662f\\u4e16\\u754c\\u4e0a\\u6700\\u597d\\u7684\\uff0c\\u201c\\u4e2d\\u56fd\\u5236\\u9020\\u201d\\u653f\\u7b56\\u610f\\u5473\\u7740\\u4e2d\\u56fd\\u56fd\\u5185\\u4e00\\u6d41\\u7684\\u3001\\u59cb\\u7ec8\\u5982\\u4e00\\u7684\\u5f00\\u53d1\\u548c\\u5236\\u9020\\u8fc7\\u7a0b\\u3002\",\"index_new_product_desc\":\"\\u9664\\u4e86\\u9ad8\\u6027\\u80fd\\u4ea7\\u54c1\\uff0c\\u5229\\u52c3\\u6d77\\u5c14\\u8fd8\\u63d0\\u4f9b\\u53ef\\u9760\\u7684\\u5168\\u7403\\u670d\\u52a1\\u3002\\u8fd9\\u610f\\u5473\\u7740\\u6211\\u4eec\\u5141\\u8bb8\\u60a8\\u5b89\\u5168\\u9ad8\\u6548\\u5730\\u5de5\\u4f5c-\\u5373\\u4f7f\\u5728\\u6781\\u7aef\\u6761\\u4ef6\\u4e0b\\u3002\",\"index_recommend_product_desc\":\"\\u6211\\u4eec\\u7684\\u5c0f\\u578b\\u8d77\\u91cd\\u673a\\u7684\\u5e94\\u7528\\u662f\\u65e0\\u9650\\u7684\\u3002\\u5728\\u8fd9\\u91cc\\u4f60\\u4f1a\\u770b\\u5230\\u4e00\\u4e2a\\u56fe\\u7247\\u548c\\u89c6\\u9891\\u753b\\u5eca\\uff0c\\u4e3a\\u4f60\\u7684\\u4e0b\\u4e00\\u4efd\\u5de5\\u4f5c\\u627e\\u5230\\u7075\\u611f\\u3002\",\"index_product_url\":{\"type\":\"2\",\"title\":\"\\u4ea7\\u54c1\\u4e2d\\u5fc3\",\"targer\":\"0\",\"value\":[{\"value\":\"\"},{\"value\":\"\"},{\"value\":\"172\",\"linkName\":\"product_catalog\"},{\"value\":\"\",\"pageId\":\"\"}]},\"footer_inquiry\":\"\\u63d0\\u4f9b\\u5353\\u8d8a\\u3001\\u5168\\u9762\\u7684\\u5ba2\\u6237\\u670d\\u52a1\\u3002\\u8ba2\\u8d2d\\u524d\\uff0c\\u8bf7\\u901a\\u8fc7\\u4ee5\\u4e0b\\u65b9\\u5f0f\\u8fdb\\u884c\\u5b9e\\u65f6\\u67e5\\u8be2\\u3002\",\"footer_bg_map\":\"\\/upload\\/image\\/20210524\\/462f29766a0e8bc5efb77e53e71f9e20.png\",\"contact_bg\":\"\\/upload\\/image\\/20210526\\/e969b4003e8f095401da8b6a8a488242.jpg\"}'),

(6,'默认主题配置','theme_template_en','{\"logo\":\"\\/upload\\/image\\/20210524\\/3f78a16cb20bcc3db76ce0d06991ef7e.png\",\"background_image\":\"\\/upload\\/image\\/20210524\\/279c731486916a91380237ab4214bfc1.jpg\",\"index_about_banner\":[{\"id\":63,\"pid\":62,\"title\":\"about-banner-1.jpg\",\"url\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"size\":70971,\"type\":\"image\",\"folder\":0,\"create_time\":\"2021-05-24 11:44:07\",\"status\":1,\"typeName\":\"\\u56fe\\u7247\",\"sizeName\":\"69.31KB\",\"cover\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"rename\":true,\"shear\":false,\"search\":{\"pid\":0,\"page\":1,\"totalPage\":1,\"isLoad\":false,\"noMore\":false,\"keyword\":\"\"},\"countSizeLoading\":false},{\"id\":63,\"pid\":62,\"title\":\"about-banner-1.jpg\",\"url\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"size\":70971,\"type\":\"image\",\"folder\":0,\"create_time\":\"2021-05-24 11:44:07\",\"status\":1,\"typeName\":\"\\u56fe\\u7247\",\"sizeName\":\"69.31KB\",\"cover\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"rename\":true,\"shear\":false,\"search\":{\"pid\":0,\"page\":1,\"totalPage\":1,\"isLoad\":false,\"noMore\":false,\"keyword\":\"\"},\"countSizeLoading\":false}],\"index_about_title\":\"XX\\u54c1\\u724c\\u4ea7\\u54c1\",\"index_about_content\":\"\",\"index_about_url\":{\"type\":\"3\",\"title\":\"#\",\"targer\":\"0\",\"value\":[{\"value\":\"\"},{\"value\":\"\"},{\"value\":\"\",\"linkName\":\"news\"},{\"value\":\"#\",\"pageId\":\"181\"}]},\"index_about_desc\":\"\\u4e2d\\u56fd\\u7684\\u6280\\u672f\\u662f\\u4e16\\u754c\\u4e0a\\u6700\\u597d\\u7684\\uff0c\\u201c\\u4e2d\\u56fd\\u5236\\u9020\\u201d\\u653f\\u7b56\\u610f\\u5473\\u7740\\u4e2d\\u56fd\\u56fd\\u5185\\u4e00\\u6d41\\u7684\\u3001\\u59cb\\u7ec8\\u5982\\u4e00\\u7684\\u5f00\\u53d1\\u548c\\u5236\\u9020\\u8fc7\\u7a0b\\u3002\",\"index_new_product_desc\":\"\\u9664\\u4e86\\u9ad8\\u6027\\u80fd\\u4ea7\\u54c1\\uff0c\\u5229\\u52c3\\u6d77\\u5c14\\u8fd8\\u63d0\\u4f9b\\u53ef\\u9760\\u7684\\u5168\\u7403\\u670d\\u52a1\\u3002\\u8fd9\\u610f\\u5473\\u7740\\u6211\\u4eec\\u5141\\u8bb8\\u60a8\\u5b89\\u5168\\u9ad8\\u6548\\u5730\\u5de5\\u4f5c-\\u5373\\u4f7f\\u5728\\u6781\\u7aef\\u6761\\u4ef6\\u4e0b\\u3002\",\"index_recommend_product_desc\":\"\\u6211\\u4eec\\u7684\\u5c0f\\u578b\\u8d77\\u91cd\\u673a\\u7684\\u5e94\\u7528\\u662f\\u65e0\\u9650\\u7684\\u3002\\u5728\\u8fd9\\u91cc\\u4f60\\u4f1a\\u770b\\u5230\\u4e00\\u4e2a\\u56fe\\u7247\\u548c\\u89c6\\u9891\\u753b\\u5eca\\uff0c\\u4e3a\\u4f60\\u7684\\u4e0b\\u4e00\\u4efd\\u5de5\\u4f5c\\u627e\\u5230\\u7075\\u611f\\u3002\",\"index_product_url\":{\"type\":\"2\",\"title\":\"\\u4ea7\\u54c1\\u4e2d\\u5fc3\",\"targer\":\"0\",\"value\":[{\"value\":\"\"},{\"value\":\"\"},{\"value\":\"172\",\"linkName\":\"product_catalog\"},{\"value\":\"\",\"pageId\":\"\"}]},\"footer_inquiry\":\"\\u63d0\\u4f9b\\u5353\\u8d8a\\u3001\\u5168\\u9762\\u7684\\u5ba2\\u6237\\u670d\\u52a1\\u3002\\u8ba2\\u8d2d\\u524d\\uff0c\\u8bf7\\u901a\\u8fc7\\u4ee5\\u4e0b\\u65b9\\u5f0f\\u8fdb\\u884c\\u5b9e\\u65f6\\u67e5\\u8be2\\u3002\",\"footer_bg_map\":\"\\/upload\\/image\\/20210524\\/462f29766a0e8bc5efb77e53e71f9e20.png\",\"contact_bg\":\"\\/upload\\/image\\/20210526\\/e969b4003e8f095401da8b6a8a488242.jpg\"}');

/*Table structure for table `mk_file` */

CREATE TABLE IF NOT EXISTS `mk_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `folder` tinyint(1) NOT NULL COMMENT '0文件 1文件夹',
  `create_time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0屏蔽 1正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `mk_file` */

insert  into `mk_file`(`id`,`pid`,`title`,`url`,`size`,`type`,`folder`,`create_time`,`status`) values 

(1,0,'279c731486916a91380237ab4214bfc1.jpg','/upload/image/20210811/7fc9db45a72e017b6b628a77133baeb2.jpg',188840,'image',0,'2021-08-11 00:52:00',1),

(2,0,'664a41e1a3c3901cf52d1a23d4633059.jpg','/upload/image/20210811/b6e3e3158ed1115cf56f5342dbe41d58.jpg',210696,'image',0,'2021-08-11 00:52:14',1);

/*Table structure for table `mk_themes` */

CREATE TABLE IF NOT EXISTS `mk_themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1.响应式 2.手机 3.电脑',
  `config` text NOT NULL COMMENT '取值为mk_config表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `mk_themes` */

INSERT  INTO `mk_themes`(`id`,`name`,`title`,`cover`,`price`,`type`,`config`) values 

(1,'template','默认模板','/upload/image/20210524/5516a8de760ecaba576f6a468ede291e.png',0.00,1,'[{\"title\":\"\\u56fe\\u7247\\u4e0a\\u4f20\",\"name\":\"imageUpload\",\"bind\":\"mk-file-select\",\"type\":\"image\",\"value\":\"\\/upload\\/image\\/20210524\\/3f78a16cb20bcc3db76ce0d06991ef7e.png\",\"field\":\"logo\",\"label\":\"\\u56fe\\u7247logo\"},{\"title\":\"\\u56fe\\u7247\\u4e0a\\u4f20\",\"name\":\"imageUpload\",\"bind\":\"mk-file-select\",\"type\":\"image\",\"value\":\"\\/upload\\/image\\/20210524\\/279c731486916a91380237ab4214bfc1.jpg\",\"field\":\"background_image\",\"label\":\"\\u5168\\u5c40\\u80cc\\u666f\\u56fe\"},{\"title\":\"\\u56fe\\u7247\\u5217\\u8868\",\"name\":\"imageList\",\"bind\":\"mk-file-list-select\",\"type\":\"image\",\"value\":[{\"id\":63,\"pid\":62,\"title\":\"about-banner-1.jpg\",\"url\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"size\":70971,\"type\":\"image\",\"folder\":0,\"create_time\":\"2021-05-24 11:44:07\",\"status\":1,\"typeName\":\"\\u56fe\\u7247\",\"sizeName\":\"69.31KB\",\"cover\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"rename\":true,\"shear\":false,\"search\":{\"pid\":0,\"page\":1,\"totalPage\":1,\"isLoad\":false,\"noMore\":false,\"keyword\":\"\"},\"countSizeLoading\":false},{\"id\":63,\"pid\":62,\"title\":\"about-banner-1.jpg\",\"url\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"size\":70971,\"type\":\"image\",\"folder\":0,\"create_time\":\"2021-05-24 11:44:07\",\"status\":1,\"typeName\":\"\\u56fe\\u7247\",\"sizeName\":\"69.31KB\",\"cover\":\"\\/upload\\/image\\/20210524\\/45e5829d012383733be3ec3699ab337c.jpg\",\"rename\":true,\"shear\":false,\"search\":{\"pid\":0,\"page\":1,\"totalPage\":1,\"isLoad\":false,\"noMore\":false,\"keyword\":\"\"},\"countSizeLoading\":false}],\"field\":\"index_about_banner\",\"label\":\"\\u4e3b\\u9875\\u5173\\u4e8e\\u6211\\u4eec\\u5e7b\\u706f\\u7247\"},{\"title\":\"\\u5b57\\u7b26\",\"name\":\"text\",\"bind\":\"el-input\",\"value\":\"OneKeyAdmin\\u54c1\\u724c\\u4ea7\\u54c1\",\"field\":\"index_about_title\",\"label\":\"\\u4e3b\\u9875\\u5173\\u4e8e\\u6211\\u4eec\\u6807\\u9898\"},{\"title\":\"\\u8bbe\\u7f6e\\u94fe\\u63a5\",\"name\":\"link\",\"bind\":\"mk-link\",\"value\":{\"type\":\"3\",\"title\":\"#\",\"targer\":\"0\",\"value\":[{\"value\":\"\"},{\"value\":\"\"},{\"value\":\"\",\"linkName\":\"news\"},{\"value\":\"#\",\"pageId\":\"235\"}]},\"field\":\"index_about_url\",\"label\":\"\\u4e3b\\u9875\\u5173\\u4e8e\\u6211\\u4eec\\u94fe\\u63a5\"},{\"title\":\"\\u6587\\u672c\\u57df\",\"name\":\"textarea\",\"bind\":\"el-input\",\"type\":\"textarea\",\"value\":\"\\u4e2d\\u56fd\\u7684\\u6280\\u672f\\u662f\\u4e16\\u754c\\u4e0a\\u6700\\u597d\\u7684\\uff0c\\u201c\\u4e2d\\u56fd\\u5236\\u9020\\u201d\\u653f\\u7b56\\u610f\\u5473\\u7740\\u4e2d\\u56fd\\u56fd\\u5185\\u4e00\\u6d41\\u7684\\u3001\\u59cb\\u7ec8\\u5982\\u4e00\\u7684\\u5f00\\u53d1\\u548c\\u5236\\u9020\\u8fc7\\u7a0b\\u3002\",\"field\":\"index_about_desc\",\"label\":\"\\u4e3b\\u9875\\u5173\\u4e8e\\u6211\\u4eec\\u63cf\\u8ff0\"},{\"title\":\"\\u7f16\\u8f91\\u5668\",\"name\":\"editor\",\"bind\":\"mk-editor\",\"value\":\"<p>LTM \\u8d77\\u91cd\\u673a\\u5728\\u5176\\u5168\\u5730\\u5f62\\u5e95\\u76d8\\u4e0a\\u4e5f\\u53ef\\u79fb\\u52a8\\u8d8a\\u91ce\\u3002 \\u6781\\u5176\\u7d27\\u51d1\\u7684\\u5c3a\\u5bf8\\u662f\\u5229\\u52c3\\u6d77\\u5c14 LTC \\u7d27\\u51d1\\u578b\\u8d77\\u91cd\\u673a\\u7684\\u4e00\\u4e2a\\u7279\\u70b9\\u3002 LTF \\u8f66\\u8f7d\\u8d77\\u91cd\\u673a\\u91c7\\u7528\\u6807\\u51c6\\u5361\\u8f66\\u5e95\\u76d8\\uff0c\\u662f\\u8d1f\\u8f7d\\u80fd\\u529b\\u9ad8\\u8fbe 60 \\u5428\\u7684\\u4f4e\\u6210\\u672c\\u66ff\\u4ee3\\u65b9\\u6848\\u3002 \\u6211\\u4eec\\u7684 LG \\u6841\\u67b6\\u81c2\\u8d77\\u91cd\\u673a\\u53ef\\u4ee5\\u5904\\u7406\\u975e\\u5e38\\u91cd\\u7684\\u8d1f\\u8f7d\\u4ee5\\u53ca\\u5de8\\u5927\\u7684\\u5de5\\u4f5c\\u9ad8\\u5ea6\\u548c\\u534a\\u5f84\\u3002 \\u6839\\u636e\\u8981\\u6c42\\uff0c\\u6211\\u4eec\\u8fd8\\u5c06\\u521b\\u5efa\\u4e13\\u95e8\\u9488\\u5bf9\\u60a8\\u7684\\u9700\\u6c42\\u91cf\\u8eab\\u5b9a\\u5236\\u7684\\u57fa\\u7840\\u548c\\u9ad8\\u7ea7\\u57f9\\u8bad\\u8bfe\\u7a0b\\u3002<\\/p>\\n<p>&nbsp;<\\/p>\\n<p>\\u5982\\u679c\\u60a8\\u6709\\u5174\\u8da3\\uff0c\\u8bf7\\u4e0e\\u60a8\\u5f53\\u5730\\u7684\\u9500\\u552e\\u5408\\u4f5c\\u4f19\\u4f34\\u8054\\u7cfb\\u3002 \\u8bad\\u7ec3\\u6709\\u7d20\\u7684\\u4e13\\u5bb6\\u662f\\u6bcf\\u5bb6\\u516c\\u53f8\\u6210\\u529f\\u7684\\u5173\\u952e\\u3002 \\u6211\\u4eec\\u4e13\\u4e1a\\u7684\\u57f9\\u8bad\\u8bfe\\u7a0b\\u8303\\u56f4\\u4f7f\\u6211\\u4eec\\u80fd\\u591f\\u5e2e\\u52a9\\u60a8\\u7684\\u4eba\\u5458\\u6269\\u5c55\\u4ed6\\u4eec\\u7684\\u4e13\\u4e1a\\u77e5\\u8bc6\\u5e76\\u5efa\\u7acb\\u5b9e\\u8df5\\u7ecf\\u9a8c\\u3002 \\u4e3a\\u4e86\\u63d0\\u9ad8\\u77e5\\u8bc6\\u8f6c\\u79fb\\uff0c\\u6211\\u4eec\\u6839\\u636e\\u4e13\\u4e1a\\u56e2\\u4f53\\u548c\\u4ee5\\u524d\\u7684\\u7ecf\\u9a8c\\u6765\\u7ec4\\u7ec7\\u6211\\u4eec\\u7684\\u8bfe\\u7a0b\\u3002 \\u7ecf\\u9a8c\\u4e30\\u5bcc\\u7684\\u5229\\u52c3\\u6d77\\u5c14\\u57f9\\u8bad\\u5e08\\u786e\\u4fdd\\u65e5\\u5e38\\u5de5\\u4f5c\\u4e2d\\u7684\\u7279\\u5b9a\\u95ee\\u9898\\u548c\\u95ee\\u9898\\u4e5f\\u80fd\\u5f97\\u5230\\u89e3\\u51b3\\u3002<\\/p>\",\"field\":\"index_about_content\",\"label\":\"\\u4e3b\\u9875\\u5173\\u4e8e\\u6211\\u4eec\\u63cf\\u8ff0\"},{\"title\":\"\\u6587\\u672c\\u57df\",\"name\":\"textarea\",\"bind\":\"el-input\",\"type\":\"textarea\",\"value\":\"\\u9664\\u4e86\\u9ad8\\u6027\\u80fd\\u4ea7\\u54c1\\uff0c\\u5229\\u52c3\\u6d77\\u5c14\\u8fd8\\u63d0\\u4f9b\\u53ef\\u9760\\u7684\\u5168\\u7403\\u670d\\u52a1\\u3002\\u8fd9\\u610f\\u5473\\u7740\\u6211\\u4eec\\u5141\\u8bb8\\u60a8\\u5b89\\u5168\\u9ad8\\u6548\\u5730\\u5de5\\u4f5c-\\u5373\\u4f7f\\u5728\\u6781\\u7aef\\u6761\\u4ef6\\u4e0b\\u3002\",\"field\":\"index_new_product_desc\",\"label\":\"\\u65b0\\u4ea7\\u54c1\\u63cf\\u8ff0\"},{\"title\":\"\\u6587\\u672c\\u57df\",\"name\":\"textarea\",\"bind\":\"el-input\",\"type\":\"textarea\",\"value\":\"\\u6211\\u4eec\\u7684\\u5c0f\\u578b\\u8d77\\u91cd\\u673a\\u7684\\u5e94\\u7528\\u662f\\u65e0\\u9650\\u7684\\u3002\\u5728\\u8fd9\\u91cc\\u4f60\\u4f1a\\u770b\\u5230\\u4e00\\u4e2a\\u56fe\\u7247\\u548c\\u89c6\\u9891\\u753b\\u5eca\\uff0c\\u4e3a\\u4f60\\u7684\\u4e0b\\u4e00\\u4efd\\u5de5\\u4f5c\\u627e\\u5230\\u7075\\u611f\\u3002\",\"field\":\"index_recommend_product_desc\",\"label\":\"\\u63a8\\u8350\\u4ea7\\u54c1\\u63cf\\u8ff0\"},{\"title\":\"\\u6587\\u672c\\u57df\",\"name\":\"textarea\",\"bind\":\"el-input\",\"type\":\"textarea\",\"value\":\"\\u6211\\u4eec\\u7684 Maeda \\u5c0f\\u578b\\u5c65\\u5e26\\u5f0f\\u8d77\\u91cd\\u673a\\u7cfb\\u5217\\u65e8\\u5728\\u5373\\u4f7f\\u5728\\u6700\\u96be\\u4ee5\\u8fdb\\u5165\\u7684\\u533a\\u57df\\u4e5f\\u80fd\\u8fdb\\u884c\\u8d77\\u91cd\\u5de5\\u4f5c\\u3002\",\"field\":\"index_hot_product_desc\",\"label\":\"\\u5c0f\\u578b\\u5c65\\u5e26\\u8d77\\u91cd\\u673a\\u63cf\\u8ff0\"},{\"title\":\"\\u8bbe\\u7f6e\\u94fe\\u63a5\",\"name\":\"link\",\"bind\":\"mk-link\",\"value\":{\"type\":\"2\",\"title\":\"\\u4ea7\\u54c1\\u4e2d\\u5fc3\",\"targer\":\"0\",\"value\":[{\"value\":\"\"},{\"value\":\"\"},{\"value\":\"172\",\"linkName\":\"product_catalog\"},{\"value\":\"\",\"pageId\":\"\"}]},\"field\":\"index_product_url\",\"label\":\"\\u4ea7\\u54c1\\u94fe\\u63a5\"},{\"title\":\"\\u6587\\u672c\\u57df\",\"name\":\"textarea\",\"bind\":\"el-input\",\"type\":\"textarea\",\"value\":\"\\u63d0\\u4f9b\\u5353\\u8d8a\\u3001\\u5168\\u9762\\u7684\\u5ba2\\u6237\\u670d\\u52a1\\u3002\\u8ba2\\u8d2d\\u524d\\uff0c\\u8bf7\\u901a\\u8fc7\\u4ee5\\u4e0b\\u65b9\\u5f0f\\u8fdb\\u884c\\u5b9e\\u65f6\\u67e5\\u8be2\\u3002\",\"field\":\"footer_inquiry\",\"label\":\"\\u5e95\\u90e8\\u8be2\\u76d8\\u4ecb\\u7ecd\"},{\"title\":\"\\u56fe\\u7247\\u4e0a\\u4f20\",\"name\":\"imageUpload\",\"bind\":\"mk-file-select\",\"type\":\"image\",\"value\":\"\\/upload\\/image\\/20210524\\/462f29766a0e8bc5efb77e53e71f9e20.png\",\"field\":\"footer_bg_map\",\"label\":\"\\u5e95\\u90e8\\u5730\\u56fe\\u80cc\\u666f\"},{\"title\":\"\\u56fe\\u7247\\u4e0a\\u4f20\",\"name\":\"imageUpload\",\"bind\":\"mk-file-select\",\"type\":\"image\",\"value\":\"\\/upload\\/image\\/20210526\\/e969b4003e8f095401da8b6a8a488242.jpg\",\"field\":\"contact_bg\",\"label\":\"\\u8054\\u7cfb\\u6211\\u4eec\\u80cc\\u666f\\u56fe\"}]');

/*Table structure for table `mk_user` */

CREATE TABLE IF NOT EXISTS `mk_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `cover` varchar(255) NOT NULL COMMENT '头像',
  `describe` varchar(255) NOT NULL COMMENT '签名',
  `birthday` date NOT NULL COMMENT '生日',
  `now_integral` int(11) NOT NULL COMMENT '当前积分',
  `history_integral` int(11) NOT NULL COMMENT '历史积分',
  `balance` decimal(12,2) NOT NULL COMMENT '余额',
  `pay_paasword` char(6) NOT NULL COMMENT '支付密码',
  `login_ip` varchar(15) NOT NULL,
  `login_count` int(11) NOT NULL,
  `login_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `create_time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0屏蔽 1正常',
  `reason` varchar(255) DEFAULT NULL COMMENT '屏蔽原因',
  `see` int(11) NOT NULL,
  `field` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `mk_user` */

/*Table structure for table `mk_user_group` */

CREATE TABLE IF NOT EXISTS `mk_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `integral` int(11) NOT NULL COMMENT '需要多少积分才能到达',
  `default` tinyint(1) NOT NULL COMMENT '1默认（会员注册默认）',
  `status` tinyint(1) NOT NULL COMMENT '0屏蔽 1正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `mk_user_group` */

insert  into `mk_user_group`(`id`,`pid`,`title`,`integral`,`default`,`status`) values 

(1,0,'VIP4',1000,0,1),

(2,0,'VIP3',100,0,1),

(3,0,'VIP2',20,0,1),

(4,0,'VIP1',0,1,1);

/*Table structure for table `mk_user_log` */

CREATE TABLE IF NOT EXISTS `mk_user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户日志',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `number` decimal(12,2) NOT NULL COMMENT '数量',
  `explain` varchar(255) NOT NULL COMMENT '说明',
  `inc` tinyint(1) NOT NULL COMMENT '1增加 0减少',
  `type` tinyint(1) NOT NULL COMMENT '0积分 1人民币',
  `create_time` datetime NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `mk_user_log` */

/*Table structure for table `mk_admin_user_token` */

CREATE TABLE IF NOT EXISTS `mk_admin_user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(60) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `mk_admin_user_token` */

/*Table structure for table `mk_user_token` */

CREATE TABLE IF NOT EXISTS `mk_user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(60) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `mk_user_token` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
