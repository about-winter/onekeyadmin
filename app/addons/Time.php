<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\addons;
/**
 * 时间组件
 */
class Time
{
	/**
	 * 时间秒转换为 00:00:00 格式
	 * @param  int  $times  秒
	 * @return string
	 */
	public static function secToTime(int $times): string
	{  
	    $result = '00:00:00';  
	    if ($times>0) {  
	            $hour = floor($times/3600); 
	            if($hour<10){
	                $hour = "0".$hour;
	            } 
	            $minute = floor(($times-3600 * $hour)/60); 
	            if($minute<10){
	                $minute = "0".$minute;
	            } 
	            $second = floor((($times-3600 * $hour) - 60 * $minute) % 60); 
	             if($second<10){
	                $second = "0".$second;
	            } 
	            $result = $hour.':'.$minute.':'.$second;  
	    }  
	    return $result;  
	}

	/**
	 * 友好时间显示
	 * @param $time
	 * @return string
	 */
	public static function friendDate($time): string
	{
	    $text = '';
	    $time = $time === NULL || $time > time() ? time() : intval($time);
	    $t = time() - $time; //时间差 （秒）
	    $y = date('Y', $time)-date('Y', time());//是否跨年
	    switch($t){
	        case $t === 0:
	        $text = '刚刚';
	        break;
	        case $t < 60:
	        $text = $t . '秒前'; // 一分钟内
	        break;
	        case $t < 60 * 60:
	        $text = floor($t / 60) . '分钟前'; //一小时内
	        break;
	        case $t < 60 * 60 * 24:
	        $text = floor($t / (60 * 60)) . '小时前'; // 一天内
	        break;
	        case $t < 60 * 60 * 24 * 3:
	        $text = floor($time/(60*60*24)) ==1 ?'昨天 ' . date('H:i', $time) : '前天 ' . date('H:i', $time) ; //昨天和前天
	        break;
	        case $t < 60 * 60 * 24 * 30:
	        $text = date('m月d日 H:i', $time); //一个月内
	        break;
	        case $t < 60 * 60 * 24 * 365&&$y==0:
	        $text = date('m月d日', $time); //一年内
	        break;
	        default:
	        $text = date('Y年m月d日', $time); //一年以前
	        break;
	    }
	    return $text;
	}
}