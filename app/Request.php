<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app;

/**
 * 应用请求对象类
 */
class Request extends \think\Request
{
	/**
	 * 管理系统地址
	 */
	public function adminUrl()
	{
		return request()->root(true) .'/'. env('map_admin');
	}

	/**
	 * 管理系统最后访问地址
	 */
	public function adminLastUrl()
	{
		$lastUrl = cookie('admin_last_url');
	    if ($lastUrl === 'index/index' || empty($lastUrl)) {
	        // 外壳
	        return request()->adminUrl();
	    } else {
	        if (count(explode('/', $lastUrl)) == 2) {
	            // 系统
	            return (string)url($lastUrl);
	        } else {
	            // 插件
	            return str_replace('/index.html', '', (string)url('plugins/index', ['path' => $lastUrl]));
	        }
	    }
	}

	/**
	 * 前端最后访问地址
	 */
	public function indexLastUrl(): string
	{
		$index_last_url = cookie('index_last_url');
	    $index_last_url = empty($index_last_url) ? get_url('user/index') : $index_last_url;
	    cookie('index_last_url',null);
	    return $index_last_url;
	}

	/**
	 * 浏览器
	 */
	public function browser()
	{
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$agent = $_SERVER['HTTP_USER_AGENT'];
			if( (false == strpos($agent,'MSIE')) && (strpos($agent, 'Trident')!==FALSE) ){
				return 'IE11.0';
			}
			if(false!==strpos($agent,'MSIE 10.0')){
				return 'IE10.0';
			}
			if(false!==strpos($agent,'MSIE 9.0')){
				return 'IE9.0';
			}
			if(false!==strpos($agent,'MSIE 8.0')){
				return 'IE8.0';
			}
			if(false!==strpos($agent,'MSIE 7.0')){
				return 'IE7.0';
			}
			if(false!==strpos($agent,'MSIE 6.0')){
				return 'IE6.0';
			}
			if(false!==strpos($agent,'Edge')){
				return 'Edge';
			}
			if(false!==strpos($agent,'Firefox')){
				return 'Firefox';
			}
			if(false!==strpos($agent,'Chrome')){
				return 'Chrome';
			}
			if(false!==strpos($agent,'Safari')){
				return 'Safari';
			}
			if(false!==strpos($agent,'Opera')){
				return 'Opera';
			}
			if(false!==strpos($agent,'360SE')){
				return '360SE';
			}
			if(false!==strpos($agent,'MicroMessage')){
				return 'weChat';
			}
			return '未知';
		} else {
			return '未知';
		}
	}

	/**
	 * 来源网站、搜索词
	 */
	public function keyword()
	{
		$name ='';
		$from = '';
		if (isset($_SERVER["HTTP_REFERER"])) {
			$referer = $_SERVER["HTTP_REFERER"];
		    if (strstr( $referer, 'baidu.com')) {
		        $name = 'www.baidu.com';
		        $from = '百度';
		    } elseif(strstr( $referer, 'google.com') or strstr( $referer, 'google.cn')) {
		        $name = 'www.google.com.hk';
		        $from = '谷歌';
		    } elseif(strstr( $referer, 'so.com')) {
		        $name = 'www.so.com';
		        $from = '360'; 
		    } elseif(strstr( $referer, 'sogou.com')) {
		        $name = 'www.sogou.com';
		        $from = '搜狗';
		    } elseif(strstr( $referer, 'facebook.com')) {
		        $name = 'www.facebook.com';
		        $from = 'facebook';
		    } elseif(strstr( $referer, 'bing.com')) {
		        $name = 'www.bing.com';
		        $from = '必应';
		    } elseif(strstr( $referer, 'yahoo.com')) {
		        $name = 'www.yahoo.com';
		        $from = '雅虎';
		    } elseif(strstr( $referer, 'soso.com')) {
		        $name = 'www.soso.com';
		        $from = '搜搜';
		    }
		}
	    return array('name'=>$name, 'from'=>$from);
	}

	/**
	 * 来路URL
	 */
	public function referer(){
		$domain = "";
		if (isset($_SERVER["HTTP_REFERER"])) {
			$url = $_SERVER["HTTP_REFERER"];   
			$str = str_replace("http://","",$url); 
			$strdomain = explode("/",$str);  
			$domain = $strdomain[0];
		}
		return $domain;  
	}

	/**
	 * 获取操作系统
	 */
	public function os()
	{
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$agent = $_SERVER['HTTP_USER_AGENT'];
			if (preg_match('/win/i', $agent) && strpos($agent, '95')){
				return 'Windows 95';
		    }
		    if (preg_match('/win 9x/i', $agent) && strpos($agent, '4.90')){
				return 'Windows ME';
		    }
		    if (preg_match('/win/i', $agent) && preg_match('/98/i', $agent)){
				return 'Windows 98';
		    }
		    if (preg_match('/win/i', $agent) && preg_match('/nt/i', $agent)){
				return 'Windows NT';
		    }
		    if (preg_match('/win/i', $agent) && preg_match('/nt 6.0/i', $agent)){
				return 'Windows Vista';
		    }
		    if (preg_match('/win/i', $agent) && preg_match('/nt 6.1/i', $agent)){
				return 'Windows 7';
		    }
		    if (preg_match('/win/i', $agent) && preg_match('/nt 6.2/i', $agent)){
				return 'Windows 8';
		    }
		    if(preg_match('/win/i', $agent) && preg_match('/nt 10.0/i', $agent)){
				return 'Windows 10';
		    }
		    if (preg_match('/win/i', $agent) && preg_match('/nt 5.1/i', $agent)){
				return 'Windows XP';
		    }
		    if (preg_match('/win/i', $agent) && preg_match('/nt 5/i', $agent)){
				return 'Windows 2000';
		    }
		    if (preg_match('/win/i', $agent) && preg_match('/32/i', $agent)){
				return 'Windows 32';
		    }
		    if (preg_match('/linux/i', $agent)){
		    	return 'Linux';
		    }
		    if (preg_match('/unix/i', $agent)){
				return 'Unix';
		    }
		    if (preg_match('/sun/i', $agent) && preg_match('/os/i', $agent)){
				return 'SunOS';
		    }
		    if (preg_match('/ibm/i', $agent) && preg_match('/os/i', $agent)){
				return 'IBM OS/2';
		    }
		    if (preg_match('/Mac/i', $agent) && preg_match('/PC/i', $agent)){
				return 'Macintosh';
		    }
		    if (preg_match('/PowerPC/i', $agent)){
				return 'PowerPC';
		    }
		    if (preg_match('/AIX/i', $agent)){
				return 'AIX';
		    }
		    if (preg_match('/HPUX/i', $agent)){
				return 'HPUX';
		    }
		    if (preg_match('/NetBSD/i', $agent)){
				return 'NetBSD';
		    }
		    if (preg_match('/BSD/i', $agent)){
				return 'BSD';
		    }
		    if (preg_match('/OSF1/i', $agent)){
				return 'OSF1';
		    }
		    if (preg_match('/IRIX/i', $agent)){
				return 'IRIX';
		    }
		    if (preg_match('/FreeBSD/i', $agent)){
				return 'FreeBSD';
		    }
		    if (preg_match('/teleport/i', $agent)){
				return 'teleport';
		    }
		    if (preg_match('/flashget/i', $agent)){
				return 'flashget';
		    }
		    if (preg_match('/webzip/i', $agent)){
				return 'webzip';
		    }
		    if (preg_match('/offline/i', $agent)){
				return 'offline';
		    }
		    if(strpos($agent, 'iphone')){
				return 'iphone';
		    }
		    if(strpos($agent, 'ipad')){
				return 'ipad';
		    }
		    if(strpos($agent, 'android')){
				return 'android';
		    }
		    if (stripos($agent, "SAMSUNG")!==false || stripos($agent, "Galaxy")!==false || strpos($agent, "GT-")!==false || strpos($agent, "SCH-")!==false || strpos($agent, "SM-")!==false) {
		        return 'android三星';
		    }
		    if (stripos($agent, "Huawei")!==false || stripos($agent, "Honor")!==false || stripos($agent, "H60-")!==false || stripos($agent, "H30-")!==false) {
		        return 'android华为';
		    }
		    if (stripos($agent, "Lenovo")!==false) {
		        return 'android联想';
		    }
		    if (strpos($agent, "MI-ONE")!==false || strpos($agent, "MI 1S")!==false || strpos($agent, "MI 2")!==false || strpos($agent, "MI 3")!==false || strpos($agent, "MI 4")!==false || strpos($agent, "MI-4")!==false) {
		        return 'android小米';
		    }
		    if (strpos($agent, "HM NOTE")!==false || strpos($agent, "HM201")!==false) {
		        return 'android红米';
		    }
		    if (stripos($agent, "Coolpad")!==false || strpos($agent, "8190Q")!==false || strpos($agent, "5910")!==false) {
		        return 'android酷派';
		    }
		    if (stripos($agent, "ZTE")!==false || stripos($agent, "X9180")!==false || stripos($agent, "N9180")!==false || stripos($agent, "U9180")!==false) {
		        return 'android中兴';
		    }
		    if (stripos($agent, "OPPO")!==false || strpos($agent, "X9007")!==false || strpos($agent, "X907")!==false || strpos($agent, "X909")!==false || strpos($agent, "R831S")!==false || strpos($agent, "R827T")!==false || strpos($agent, "R821T")!==false || strpos($agent, "R811")!==false || strpos($agent, "R2017")!==false) {
		        return 'androidOPPO';
		    }
		    if (strpos($agent, "HTC")!==false || stripos($agent, "Desire")!==false) {
		        return 'androidHTC';
		    }
		    if (stripos($agent, "vivo")!==false) {
		        return 'androidvivo';
		    }
		    if (stripos($agent, "K-Touch")!==false) {
		        return 'android天语';
		    }
		    if (stripos($agent, "Nubia")!==false || stripos($agent, "NX50")!==false || stripos($agent, "NX40")!==false) {
		        return 'android努比亚';
		    }
		    if (strpos($agent, "M045")!==false || strpos($agent, "M032")!==false || strpos($agent, "M355")!==false) {
		        return 'android魅族';
		    }
		    if (stripos($agent, "DOOV")!==false) {
		        return 'android朵唯';
		    }
		    if (stripos($agent, "GFIVE")!==false) {
		        return 'android基伍';
		    }
		    if (stripos($agent, "Gionee")!==false || strpos($agent, "GN")!==false) {
		        return 'android金立';
		    }
		    if (stripos($agent, "HS-U")!==false || stripos($agent, "HS-E")!==false) {
		        return 'android海信';
		    }
		    if (stripos($agent, "Nokia")!==false) {
		        return 'android诺基亚';
		    }
		    return '未知';
		} else {
			return '未知';
		}
	}
	/**
	 * 业务日志列表 
	 * @return array
	 */
	public function businessLog($personal = false)
	{
		$input = input();
	    $page  = isset($input['page']) ? (int)$input['page'] : 1;
	    $pageSize = isset($input['pageSize']) ? (int)$input['pageSize'] : 20;
	    $path = config('log.channels.business.path') . '/business.log';
	    $data = [];
	    $count = 0;
	    if (is_file($path)) {
	        $log  = file_get_contents($path);
	        $log  = explode(PHP_EOL, $log);
	        // 整理日志列表
	        foreach ($log as $k => $v) {
	            if (! empty($log[$k])) {
	                $log[$k] = json_decode($log[$k]);
	                if (isset($log[$k]->msg)) {
	                    $log[$k]->msg = unserialize($log[$k]->msg);
	                    $log[$k]->msg['date'] = $log[$k]->time;
	                    // 是否为个人
	                    if ($personal) {
	                        if (request()->userInfo['id'] == $log[$k]->msg['userId']) $data[$k] = $log[$k]->msg;
	                    } else {
	                        $data[$k] = $log[$k]->msg;
	                    }
	                }
	            } else {
	                unset($log[$k]);
	            }
	        }
	        array_values($data);
	        $data = array_sort($data, 'date', 'desc');
	        // 关键词搜索
	        if (! empty($input['keyword'])) {
	            $list = [];
	            foreach ($data as $key => $val) {
	                if(strstr($val['ip'], $input['keyword']) || 
	                    strstr($val['path'], $input['keyword']) || 
	                    strstr($val['title'], $input['keyword']) ||
	                    strstr($val['userAccount'], $input['keyword'])) {
	                    array_push($list, $val);
	                }
	            }
	            $data = $list;
	        }
	        // 日期搜索
	        if (! empty($input['date'])) {
	            $list = [];
	            $date = $input['date'];
	            foreach ($data as $key => $val) {
	                if(strtotime($val['date']) > strtotime($date[0])  && strtotime($val['date']) < strtotime($date[1])) {
	                    array_push($list, $val);
	                }
	            }
	            $data = $list;
	        }
	        // 总页数
	        $count = count($data);
	        // 分页
	        $list     = [];
	        $startKey = ($page - 1) * $pageSize;
	        $endKey   = $page * $pageSize;
	        foreach ($data as $key => $val) {
	            if($key >= $startKey && $key < $endKey) {
	                array_push($list, $val);
	            }
	        }
	        $data = $list;
	    }
	    return ['data'=> $data, 'count'=>$count];
	}
}
