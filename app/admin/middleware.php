<?php
// +----------------------------------------------------------------------
// | 全局中间件定义文件
// +----------------------------------------------------------------------

return [
    // Session初始化
    \think\middleware\SessionInit::class,
    // 环境检查
    app\admin\middleware\AppCheck::class,
    // 登录检查
    app\admin\middleware\LoginCheck::class,
    // 菜单检查
    app\admin\middleware\MenuCheck::class,
    // 权限检查
    app\admin\middleware\AuthCheck::class,
    // 全局检查
    app\admin\middleware\CommonCheck::class,
    // 多语言加载
    \think\middleware\LoadLangPack::class,
    // 语言检查
    app\admin\middleware\LangCheck::class,
    // 日志写入
    app\admin\middleware\LogWriting::class,
];
