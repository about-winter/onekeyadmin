<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class File extends Validate
{
    protected $rule =   [
        'id'         => 'require|number',
        'pid'        => 'require|number',
        'type'       => 'require',
        'status'     => 'require|number',
        'ids'        => 'require|array',
        'field'      => 'require|alphaDash',
        'value'      => 'require',
        'data'       => 'require|array'
    ];
    protected $message  =   [
        'id.require'         => ['status' => 'error', 'message' => 'id不能为空'],
        'id.number'          => ['status' => 'error', 'message' => 'id只能为数字'],
        'pid.require'        => ['status' => 'error', 'message' => 'pid不能为空'],
        'pid.number'         => ['status' => 'error', 'message' => 'pid只能为数字'],
        'type.require'       => ['status' => 'error', 'message' => '类型不能为空'],
        'status.require'     => ['status' => 'error', 'message' => '状态不能为空'],
        'status.number'      => ['status' => 'error', 'message' => '状态只能为数字'],
        'ids.require'        => ['status' => 'error', 'message' => 'ids不能为空'],
        'ids.array'          => ['status' => 'error', 'message' => 'ids只能为数组'],
        'field.require'      => ['status' => 'error', 'message' => '请指定要修改的字段'],
        'field.alphaDash'    => ['status' => 'error', 'message' => '指定字段类型错误'],
        'value.require'      => ['status' => 'error', 'message' => '字段值不能为空'],
        'data.require'       => ['status' => 'error', 'message' => '粘贴内容不能为空'],
        'data.array'         => ['status' => 'error', 'message' => '粘贴内容只能为数组'],
    ];
    // 验证场景定义
    public function sceneDelOrRecycle()
    {
        return $this->only(['ids']);
    }
    public function sceneField()
    {
        return $this->only(['id', 'value', 'field']);
    }
    public function sceneAddFolder()
    {
        return $this->only(['pid','type']);
    }
    public function sceneCountFolder()
    {
        return $this->only(['id']);
    }
    public function scenePaste()
    {
        return $this->only(['data', 'pid']);
    }
}