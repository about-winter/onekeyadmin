<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class Config extends Validate
{
    protected $rule = [
        'value' => 'max:65535',
    ];
    
    protected $message = [
        'value.max' => ['status' => 'error', 'message' => '配置内容不能超过65535个字符'],
    ];

    protected $scene = [
        'value' => ['value'],
    ];
}