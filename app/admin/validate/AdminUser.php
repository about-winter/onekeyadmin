<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class AdminUser extends Validate
{
    protected $rule =   [
        'id'       => 'require|number',
        'status'   => 'require|number',
        'group_id' => 'require',
        'nickname' => 'require|min:2|max:40',
        'email'    => 'require|email',
        'account'  => [
            'require',
            'min'  => 5,
            'max'  => 40, 
        ],
        'password' => [
            'min'  => 6,
            'max'  => 40, 
            'regex'=> '/((?=.*[a-z])(?=.*\d)|(?=[a-z])(?=.*[#@!~%^&*])|(?=.*\d)(?=.*[#@!~%^&*]))[a-z\d#@!~%^&*]{8,16}/i'
        ],
        'cover'    => 'max:255',
    ];
    protected $message  =   [
        'id.require'       => ['status' => 'error', 'message' => 'id不能为空'],
        'id.number'        => ['status' => 'error', 'message' => 'id只能为数字'],
        'status.require'   => ['status' => 'error', 'message' => '状态不能为空'],
        'status.number'    => ['status' => 'error', 'message' => '状态只能为数字'],
        'group_id.require' => ['status' => 'error', 'message' => '所属组别不能为空'],
        'nickname.require' => ['status' => 'error', 'message' => '管理员昵称不能为空'],
        'nickname.min'     => ['status' => 'error', 'message' => '管理员昵称不能少于2个字符'],
        'nickname.max'     => ['status' => 'error', 'message' => '管理员昵称不能超过40个字符'],
        'email'            => ['status' => 'error', 'message' => '管理员邮箱格式不正确'],
        'account.require'  => ['status' => 'error', 'message' => '管理员账号不能为空'],
        'account.min'      => ['status' => 'error', 'message' => '管理员账号不能少于5个字符'],
        'account.max'      => ['status' => 'error', 'message' => '管理员账号不能超过40个字符'],
        'password.min'     => ['status' => 'error', 'message' => '管理员密码不能少于6个字符'],
        'password.max'     => ['status' => 'error', 'message' => '管理员密码不能超过40个字符'],
        'password.regex'   => ['status' => 'error', 'message' => '管理员密码不能是纯数字、纯字母、纯特殊字符'],
        'cover.max'        => ['status' => 'error', 'message' => '用户头像不能超过255个字符'],
        'ids.require'      => ['status' => 'error', 'message' => 'ids不能为空'],
        'ids.array'        => ['status' => 'error', 'message' => 'ids只能为数组'],
    ];
    // 验证场景定义
    public function sceneUpdate()
    {
        return $this->remove('password','require|min:6|max:40|regex');
    }
    public function sceneSave()
    {
        return $this->remove('id', 'require');
    }
    public function sceneDelete()
    {
        return $this->only(['ids'])->append('ids', 'require|array');
    }
}