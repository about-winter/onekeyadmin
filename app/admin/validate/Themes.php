<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class Themes extends Validate
{
    protected $rule =   [
        'name'        => 'require',
        'mp_template' => 'require',
        'pc_template' => 'require',
        'id'          => 'require',
        'config'      => 'require|max:65535',
    ];

    protected $message  =   [
        'name.require'        => ['status' => 'error', 'message' => '请指定插件名称'],
        'mp_template.require' => ['status' => 'error', 'message' => '请指定手机主题'],
        'pc_template.require' => ['status' => 'error', 'message' => '请指定电脑主题'],
        'id.require'          => ['status' => 'error', 'message' => '请指定主题编号'],
        'config.require'      => ['status' => 'error', 'message' => '主题配置不能为空'],
        'config.max'          => ['status' => 'error', 'message' => '主题配置值不能大于65535个字符'],
    ];

    protected $scene = [
        'delete'       => ['name'],
        'update'       => ['mp_template','pc_template'],
        'install'      => ['name','id'],
        'config'       => ['name'],
        'configUpdate' => ['name','config'],
    ];
}