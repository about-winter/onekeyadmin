<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class Plugins extends Validate
{
    protected $rule =   [
        'name'         => 'require',
        'value'        => 'require',
        'id'           => 'require',
        'install_type' => 'require',
    ];

    protected $message  =   [
        'name.require'         => ['status' => 'error', 'message' => '请指定插件名称'],
        'value.require'        => ['status' => 'error', 'message' => '请指定插件值'],
        'id.require'           => ['status' => 'error', 'message' => '请指定插件编号'],
        'install_type.require' => ['status' => 'error', 'message' => '请指定安装类型'],
    ];

    protected $scene = [
        'delete'  => ['name'],
        'update'  => ['name','value'],
        'install' => ['name','id','install_type'],
    ];
}