<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class AdminMenu extends Validate
{
    protected $rule = [
        'id'     => 'require|number',
        'status' => 'require|number',
        'pid'    => 'require|number',
        'title'  => 'require|min:2|max:100',
        'icon'   => 'min:2|max:255',
        'path'   => 'require|min:2|max:255',
        'sort'   => 'require|number',
        'ifshow' => 'require|number',
    ];
    protected $message = [
        'id.require'     => ['status' => 'error', 'message' => 'id不能为空'],
        'id.number'      => ['status' => 'error', 'message' => 'id只能为数字'],
        'status.require' => ['status' => 'error', 'message' => '状态不能为空'],
        'status.number'  => ['status' => 'error', 'message' => '状态只能为数字类型'],
        'pid.require'    => ['status' => 'error', 'message' => '父级不能为空'],
        'pid.number'     => ['status' => 'error', 'message' => '父级ID只能为数字'],
        'title.require'  => ['status' => 'error', 'message' => '菜单名称不能为空'],
        'title.min'      => ['status' => 'error', 'message' => '菜单名称不能少于2个字符'],
        'title.max'      => ['status' => 'error', 'message' => '菜单名称不能超过100个字符'],
        'icon.min'       => ['status' => 'error', 'message' => '菜单图标不能少于2个字符'],
        'icon.max'       => ['status' => 'error', 'message' => '菜单图标不能超过255个字符'],
        'path.require'   => ['status' => 'error', 'message' => '菜单访问路径不能为空'],
        'path.min'       => ['status' => 'error', 'message' => '菜单访问路径不能少于2个字符'],
        'path.max'       => ['status' => 'error', 'message' => '菜单访问路径不能超过255个字符'],
        'sort.require'   => ['status' => 'error', 'message' => '菜单排序不能为空'],
        'sort.number'    => ['status' => 'error', 'message' => '菜单排序只能为数字类型'],
        'ifshow.require' => ['status' => 'error', 'message' => '菜单显示不能为空'],
        'ifshow.number'  => ['status' => 'error', 'message' => '菜单显示只能为数字类型'],
        'ids.require'    => ['status' => 'error', 'message' => 'ids不能为空'],
        'ids.array'      => ['status' => 'error', 'message' => 'ids只能为数组'],
    ];
    // 验证场景定义
    public function sceneSave()
    {
        return $this->remove('id', 'require');
    }
    public function sceneDelete()
    {
        return $this->only(['ids'])->append('ids', 'require|array');
    }
}