<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class AdminGroup extends Validate
{
    protected $rule = [
        'id'     => 'require|number',
        'status' => 'require|number',
        'pid'    => 'require|number',
        'title'  => 'require|min:2|max:100',
    ];
    protected $message = [
        'pid.require'    => ['status' => 'error', 'message' => '父级不能为空'],
        'pid.number'     => ['status' => 'error', 'message' => '父级ID只能为数字'],
        'title.require'  => ['status' => 'error', 'message' => '组别名称不能为空'],
        'title.min'      => ['status' => 'error', 'message' => '组别名称不能少于2个字符'],
        'title.max'      => ['status' => 'error', 'message' => '组别名称不能超过100个字符'],
        'id.require'     => ['status' => 'error', 'message' => 'id不能为空'],
        'id.number'      => ['status' => 'error', 'message' => 'id只能为数字'],
        'status.require' => ['status' => 'error', 'message' => '状态不能为空'],
        'status.number'  => ['status' => 'error', 'message' => '状态只能为数字'],
        'ids.require'    => ['status' => 'error', 'message' => 'ids不能为空'],
        'ids.array'      => ['status' => 'error', 'message' => 'ids只能为数组'],
    ];
    // 验证场景定义
    public function sceneSave()
    {
        return $this->remove('id', 'require');
    }
    public function sceneDelete()
    {
        return $this->only(['ids'])->append('ids', 'require|array');
    }
}