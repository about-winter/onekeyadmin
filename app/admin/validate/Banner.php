<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class Banner extends Validate
{
    protected $rule =   [
        'id'         => 'require|number',
        'status'     => 'require|number',
        'catalog_id' => 'max:255',
        'cover'      => 'require',
        'content'    => 'max:65535',
        'link'       => 'max:65535',
    ];
    protected $message  =   [
        'id.require'       => ['status' => 'error', 'message' => 'id不能为空'],
        'id.number'        => ['status' => 'error', 'message' => 'id只能为数字'],
        'status.require'   => ['status' => 'error', 'message' => '状态不能为空'],
        'status.number'    => ['status' => 'error', 'message' => '状态只能为数字'],
        'catalog_id.max'   => ['status' => 'error', 'message' => '分类选择不能超过255个字符'],
        'cover.require'    => ['status' => 'error', 'message' => 'banner图片不能为空'],
        'content.max'      => ['status' => 'error', 'message' => 'banner内容不能超过65535个字符'],
        'link.max'         => ['status' => 'error', 'message' => 'link内容不能超过65535个字符'],
        'ids.require'      => ['status' => 'error', 'message' => 'ids不能为空'],
        'ids.array'        => ['status' => 'error', 'message' => 'ids只能为数组'],
    ];
    // 验证场景定义
    public function sceneSave()
    {
        return $this->remove('id', 'require');
    }
    public function sceneDelete()
    {
        return $this->only(['ids'])->append('ids', 'require|array');
    }
}