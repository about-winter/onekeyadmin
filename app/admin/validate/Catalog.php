<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class Catalog extends Validate
{
    protected $rule = [
        'id'                  => 'require|number',
        'status'              => 'require|number',
        'pid'                 => 'require|number',
        'title'               => 'require|min:2|max:200',
        'cover'               => 'max:255',
        'content'             => 'max:65535',
        'links_value'         => 'max:65535',
        'seo_url'             => 'min:3|max:255',
        'seo_title'           => 'max:255',
        'seo_keywords'        => 'max:255',
        'seo_description'     => 'max:255',
    ];
    protected $message = [
        'id.require'             => ['status' => 'error', 'message' => 'id不能为空'],
        'id.number'              => ['status' => 'error', 'message' => 'id只能为数字'],
        'status.require'         => ['status' => 'error', 'message' => '状态不能为空'],
        'status.number'          => ['status' => 'error', 'message' => '状态只能为数字类型'],
        'pid.require'            => ['status' => 'error', 'message' => '父级不能为空'],
        'pid.number'             => ['status' => 'error', 'message' => '父级ID只能为数字'],
        'title.require'          => ['status' => 'error', 'message' => '名称不能为空'],
        'title.min'              => ['status' => 'error', 'message' => '名称不能少于2个字符'],
        'title.max'              => ['status' => 'error', 'message' => '名称不能超过200个字符'],
        'cover.max'              => ['status' => 'error', 'message' => '封面地址不能超过255个字符'],
        'content.max'            => ['status' => 'error', 'message' => '内容不能超过65535个字符'],
        'seo_url.min'            => ['status' => 'error', 'message' => '自定义地址不能小于3个字符'],
        'seo_url.max'            => ['status' => 'error', 'message' => '自定义地址不能超过255个字符'],
        'links_value.max'        => ['status' => 'error', 'message' => '链接地址不能超过65535个字符'],
        'seo_title.max'          => ['status' => 'error', 'message' => '页面标题不能超过255个字符'],
        'seo_keywords.max'       => ['status' => 'error', 'message' => '页面关键字不能超过255个字符'],
        'seo_description.max'    => ['status' => 'error', 'message' => '页面描述不能超过255个字符'],
        'ids.require'            => ['status' => 'error', 'message' => 'ids不能为空'],
        'ids.array'              => ['status' => 'error', 'message' => 'ids只能为数组'],
    ];
    // 验证场景定义
    public function sceneSave()
    {
        return $this->remove('id', 'require');
    }
    public function sceneDelete()
    {
        return $this->only(['ids'])->append('ids', 'require|array');
    }
    public function sceneSynchro()
    {
        return $this->only(['ids'])->append('ids', 'require|array');
    }
}