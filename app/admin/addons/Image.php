<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\addons;

use think\Image as thinkImage;
/**
 * 图片组件
 */
class Image
{
    /**
     * 生成缩略图
     */
    public static function thumb(string $url, $width=40, $height=40)
    {
        $public     = public_path();
        $file       = str_replace('\/', '/', $public . $url);
        $fileName   = pathinfo($url, PATHINFO_FILENAME);
        $thumbName  = str_replace($fileName, $fileName.$width.'x'.$height, $file);
        $image      = thinkImage::open($file);
        $image->thumb($width, $height)->save($thumbName);
    }
}