<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin\addons;

use think\facade\Db;
use app\addons\Email;
use app\admin\model\Config;
use app\admin\model\AdminUser;
/**
 * 登录组件
 */
class Login
{
    // token撒盐
    private static $salt = '513038996@qq.com';

    /**
     * 清除登录状态
     */
    public static function clearLoginStatus()
    {
        session('admin_user', null);
        cookie('admin_token', null);
        cookie('admin_last_url', null);
    }

    /**
     * 获取当前ip登录错误次数
     */
    public static function getLoginErrorNum(): int
    {
        $admin_error_num  = session('admin_error_num');
        return empty($admin_error_num) ? 0 : $admin_error_num;
    }

    /**
     * 设置当前ip登录错误次数
     */
    public static function setLoginErrorNum(): void
    {
        $get_num    = self::getLoginErrorNum();
        $admin_error_num  = $get_num + 1;
        session('admin_error_num', $admin_error_num);
    }

    /**
     * 清除当前ip登录错误次数
     */
    public static function clearLoginErrorNum(): void
    {
        session('admin_error_num', null);
    }

    /**
     * 是否开启自动登录验证
     */
    public static function openAutomaticLogin(array $input): void
    {
        $userInfo = session('admin_user');
        $key   = $userInfo->account . self::$salt . request()->ip();
        $token = password_hash($key, PASSWORD_BCRYPT, ['cost' => 12]);
        $time  = 14*24*3600;
        if ($input['checked']) {
            $bool = Db::name('admin_user_token')->save([
                'user_id' => $userInfo->id, 
                'token' => $token, 
                'create_time' => date('Y-m-d H:i:s')
            ]);
            if ($bool) {
                cookie('admin_token', $token, $time);
            }
        } else {
            cookie('admin_token', null);
        }
    }

    /**
     * 判断自动登录的token
     */
    public static function checkAutomaticLogin(): void
    {
        $userInfo = session('admin_user');
        if (empty($userInfo)) {
            $token = cookie('admin_token');
            if ($token) {
                $time   = 14*24;
                $userId = Db::name("admin_user_token")->where("token", $token)->whereTime("create_time","-$time hours")->value('user_id');
                if ($userId) {
                    $userInfo = AdminUser::with(['group'])->where('status', 1)->where('id', $userId)->find();
                    if ($userInfo) {
                        session('admin_user',$userInfo);
                    }
                }
            }
        }
    }

    /**
     * 发送邮箱验证码
     */
    public static function sendEmailCode(string $email, string $name, string $operation): array
    {
        $value = session($name);
        if (! empty($value)) {
            $interval = 60;
            $lasttime = unserialize($value)['time'];
            if (time() - $lasttime < $interval) {
                return ['status'=>'error','message'=>'获取过于频繁，请等待一分钟后再试'];
            }
        }
        $basics = Config::getVal('basics');
        $title  = $basics['seo_title'];
        $code   = rand(1000,9999);
        $body   = $operation."验证<br/>您好".$email."!<br/> ".$title."，请将验证码填写到".$operation."。<br/>验证码：".$code."";
        $res    = Email::send($email,$title,$body);
        if ($res['status'] === 'success') {
            $arr['code'] = $code;
            $arr['time'] = time();
            $session     = serialize($arr);
            session($name, $session);
        }
        return $res;
    }

    /**
     * 获取邮箱验证码
     */
    public static function getEmailCode(string $name): ?int
    {
        $value = session($name);
        session($name, null);
        return empty($value) ? null : unserialize($value)['code'];
    }
}