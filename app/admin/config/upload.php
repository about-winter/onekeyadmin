<?php
// +----------------------------------------------------------------------
// | 上传限制
// +----------------------------------------------------------------------

return [
    'ext'  => [
        'image' => 'png,jpg,jpeg,bmp,gif,ico',
        'video' => 'mp4',
        'audio' => 'mp3',
        'word'  => 'docx,doc',
        'other' => 'swf,psd,css,js,html,exe,dll,zip,rar,ppt,pdf,xlsx,xls,txt,torrent,dwt,sql',
    ],
    'size' => [
        'image' => 1000*1024*1024,
        'video' => 1000*1024*1024,
        'audio' => 1000*1024*1024,
        'other' => 1000*1024*1024,
        'word'  => 1000*1024*1024,
    ],
];
