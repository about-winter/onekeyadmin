<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use PDO;
use think\facade\View;
use think\facade\Cache;
use app\addons\File;
use app\admin\BaseController;
/**
 * 外壳
 */
class Index extends BaseController
{
    public function index()
    {
        // 系统通知
        $notification = cache('notification');
        if (empty($notification)) {
            $res = api_post('system/notification');
            $notification = $res['status'] === 'success' ? $res['data'] : [];
            cache('notification', $notification);
        }
        View::assign([
            'menus'        => $this->request->publicMenu,
            'notification' => $notification,
        ]);
        return View::fetch();
    }

    /**
     * 清除缓存
     */
    public function cacheClear()
    {
        Cache::clear();
        return json(['status' => 'success', 'message' => '清除成功']);
    }

    /**
     * 检查更新
     */
    public function checkUpdate()
    {
        if ($this->request->isPost()) {
            $data['version'] = config('app.version');
            $res = api_post('system/checkUpdate', $data);
            return json($res);
        }
    }

    /**
     * 更新系统
     */
    public function update()
    {
        if ($this->request->isPost()) {
            if (is_writable(app_path())) {
                $data['version'] = input('version');
                $data['now_version'] = config('app.version');
                $res = api_post('system/getFile', $data);
                if ($res['status'] === 'success') {
                    $path = root_path().'install/';
                    // 创建文件
                    File::create($path.'file.zip', base64_decode($res['data']['zip']));
                    // 执行解压
                    File::extract($path.'file.zip', root_path());
                    // 执行数据
                    if (is_file(root_path() . 'file.sql')) {
                        $sql  = file_get_contents(root_path() . 'file.sql');
                        $info = "mysql:dbname=".env('database.database').";host=".env('database.hostname')."";
                        $db   = new PDO($info, env('database.username'), env('database.password'));
                        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
                        $db->exec($sql);
                    }
                    // 修改版本
                    $pat[] = 'version';
                    $rep[] = $res['data']['version'];
                    File::editConfig($pat, $rep, config_path() . 'app.php');
                    // 初始化文件
                    $initFile = root_path() . 'init.php';
                    if (is_file($initFile)) {
                        $bool = include($initFile);
                        if ($bool) {
                            unlink($initFile);
                        }
                    }
                    return json(['status' => 'success', 'message' => '更新成功', 'version' => $res['data']['version']]);
                } else {
                    return json($res);
                }
            } else {
                return json(['status' => 'error', 'message' => '应用目录没有写入权限']);
            }
        }
    }
}
