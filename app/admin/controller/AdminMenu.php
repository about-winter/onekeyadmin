<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\View;
use think\exception\ValidateException;
use app\admin\BaseController;
use app\addons\File;
use app\admin\model\AdminMenu as MenuModel;
use app\admin\validate\AdminMenu as MenuValidate;
/**
 * 管理员菜单管理
 */
class AdminMenu extends BaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        if ($this->request->isPost()) {
            return json([
                'status'     => 'success', 
                'message'    => '请求成功', 
                'data'       => $this->request->menu, 
                'publicMenu' => $this->request->publicMenu,
            ]);
        } else {
            return View::fetch();
        }
    }

    /**
     * 保存新建的资源
     */
    public function save()
    {
        try {
            $input = input();
            validate(MenuValidate::class)->scene('save')->check($input);
            $p = MenuModel::create([
                'pid'        => $input['pid'],
                'title'      => $input['title'],
                'icon'       => $input['icon'],
                'path'       => $input['path'],
                'sort'       => $input['sort'],
                'status'     => $input['status'],
                'ifshow'     => $input['ifshow'],
                'logwriting' => $input['logwriting'],
            ]);
            if (isset($input['isList']) && $input['isList'] === 1) {
                MenuModel::create([
                    'pid'        => $p['id'],
                    'title'      => $input['title'],
                    'icon'       => $input['icon'],
                    'path'       => $input['path'],
                    'sort'       => 0,
                    'status'     => 1,
                    'ifshow'     => 1,
                    'logwriting' => $input['logwriting'],
                ]);
            }
            return json(['status' => 'success', 'message' => '新增成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }

    /**
     * 保存更新的资源
     */
    public function update()
    {
        try {
            $input = input();
            if (! is_string($input['id'])) {
                validate(MenuValidate::class)->check($input);
                $save  = MenuModel::find($input['id']);
                $save->pid        = $input['pid'];
                $save->title      = $input['title'];
                $save->icon       = $input['icon'];
                $save->path       = $input['path'];
                $save->sort       = $input['sort'];
                $save->status     = $input['status'];
                $save->ifshow     = $input['ifshow'];
                $save->logwriting = $input['logwriting'];
                $save->save();
            } else {
                $path = explode('/', $input['path']);
                $name = $path[0];
                unset($path[0]);
                $path = implode('/', $path);
                $file = plugin_path() . $name . '/menu.php';
                $menu = include($file);
                foreach ($menu as $key1 => $val1) {
                    if ($path === $val1['path']) {
                        $menu[$key1]['title'] = $input['title'];
                        $menu[$key1]['status'] = $input['status'];
                        $menu[$key1]['ifshow'] = $input['ifshow'];
                        $menu[$key1]['logwriting'] = $input['logwriting'];
                    }
                    if (isset($menu[$key1]['children'])) {
                        foreach ($menu[$key1]['children'] as $key2 => $val2) {
                            if ($path === $val2['path']) {
                                $menu[$key1]['children'][$key2]['title'] = $input['title'];
                                $menu[$key1]['children'][$key2]['status'] = $input['status'];
                                $menu[$key1]['children'][$key2]['ifshow'] = $input['ifshow'];
                                $menu[$key1]['children'][$key2]['logwriting'] = $input['logwriting'];
                            }
                            if (isset($menu[$key1]['children'][$key2]['children'])) {
                                foreach ($menu[$key1]['children'][$key2]['children'] as $key3 => $val3) {
                                    if ($path === $val3['path']) {
                                        $menu[$key1]['children'][$key2]['children'][$key3]['title'] = $input['title'];
                                        $menu[$key1]['children'][$key2]['children'][$key3]['status'] = $input['status'];
                                        $menu[$key1]['children'][$key2]['children'][$key3]['ifshow'] = $input['ifshow'];
                                        $menu[$key1]['children'][$key2]['children'][$key3]['logwriting'] = $input['logwriting'];
                                    }
                                }
                            }
                        }
                    }
                }
                // 写入文件
                $content = "<?php\nreturn ".var_export($menu,true).";";
                File::create($file, $content);
            }
            return json(['status' => 'success', 'message' => '修改成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }

    /**
     * 删除指定资源
     */
    public function delete()
    {
        try {
            $input = input();
            validate(MenuValidate::class)->scene('delete')->check($input);
            MenuModel::recursiveDestroy($input['ids']);
            return json(['status' => 'success', 'message' => '删除成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }
}
