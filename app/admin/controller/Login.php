<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\View;
use think\captcha\facade\Captcha;
use think\exception\ValidateException;
use app\admin\BaseController;
use app\admin\model\AdminUser;
use app\admin\addons\Login as LoginAddons;
use app\admin\validate\Login as LoginValidate;
/**
 * 未登录下的操作
 */
class Login  extends BaseController
{
    /**
     * 登录
     */
    public function index()
    {
        if ($this->request->isPost()) {
            try {
                $input = input();
                $scene = $this->isNeedVerification() === 1 ? 'captchalogin' : 'login';
                validate(LoginValidate::class)->scene($scene)->check($input);
                $userInfo = AdminUser::with(['group'])->withoutField('password')->where('account|email',$input['loginAccount'])->find();
                if ($userInfo) {
                    $password = AdminUser::where('id', $userInfo['id'])->value('password');
                    if (password_verify($input['loginPassword'], $password)) {
                        if ($userInfo['status'] === 1) {
                            $userInfo->login_count  = $userInfo->login_count + 1;
                            $userInfo->login_ip     = $this->request->ip();
                            $userInfo->login_time   = date('Y-m-d H:i:s');
                            $userInfo->save();
                            // 记录管理员信息
                            session('admin_user',$userInfo);
                            // 清除登录错误次数
                            LoginAddons::clearLoginErrorNum();
                            // 勾选两周内自动登录
                            LoginAddons::openAutomaticLogin($input);
                            return json(['status' => 'success', 'message' => '登录成功', 'url'=> $this->request->adminLastUrl()]);
                        } else {
                            $error = ['status' => 'error', 'message' => '账号正在审核'];
                        }
                    } else {
                        $error = ['status' => 'error', 'message' => '账号或密码错误'];
                    }
                } else {
                    $error = ['status' => 'error', 'message' => '账号或密码错误'];
                }
                // 设置当前ip登录错误次数
                LoginAddons::setLoginErrorNum();
                return json($error);
            } catch ( ValidateException $e ) {
                return json($e->getError());
            }
        } else {
            $userInfo = session('admin_user');
            if (empty($userInfo)) {
                return View::fetch();
            } else {
                return redirect($this->request->adminUrl());
            }
        }
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        LoginAddons::clearLoginStatus();
        return redirect((string)url("login/index"));
    }

    /**
     * 修改密码
     */
    public function password()
    {
        if ($this->request->isPost()) {
            try {
                $input = input();
                validate(LoginValidate::class)->scene('password')->check($input);
                $code = LoginAddons::getEmailCode('admin_password_code');
                if ($code === (int)$input['code']) {
                    $save = AdminUser::where('email',$input['email'])->find();
                    if ($save) {
                        $save->password = $input['password'];
                        $save->save();
                        LoginAddons::clearLoginStatus();
                        return json(['status' => 'success', 'message' => '修改成功']);
                    } else {
                        return json(['status' => 'error', 'message' => '该邮箱号未被注册！']);
                    }
                } else {
                    return json(["status"=>"error","message"=>"输入的验证码有误"]);
                }
            } catch ( ValidateException $e ) {
                return json($e->getError());
            }
        } else {
            return View::fetch();
        }
    }

    /**
     * 发送修改密码邮箱验证码
     */
    public function passwordEmailCode()
    {
        try {
            $input = input();
            validate(LoginValidate::class)->scene('code')->check($input);
            $email = AdminUser::where('email', $input['email'])->value('id');
            if ($email) {
                $message = LoginAddons::sendEmailCode($input['email'], 'admin_password_code', '修改密码');
                return json($message);
            } else {
                return json(['status' => 'error', 'message' => '该邮箱号未被注册！']);
            }
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }

    /**
     * 登录验证码
     */
    public function verify()
    {
        return Captcha::create();
    }

    /**
     * 是否开启登录验证码
     * @return int
     */
    public function isNeedVerification()
    {
        $error_num = LoginAddons::getLoginErrorNum();
        if ($error_num > 5) {
            return 1;
        } else {
            return 0;
        }
    }
}
