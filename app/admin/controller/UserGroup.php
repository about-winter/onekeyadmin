<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\View;
use think\exception\ValidateException;
use app\admin\BaseController;
use app\admin\validate\UserGroup as GroupValidate;
use app\admin\model\UserGroup as GroupModel;
/**
 * 用户角色管理
 */
class UserGroup extends BaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        if ($this->request->isPost()) {
            $input = input();
            $order = $input['order'] === 'ascending' ? 'asc' : 'desc';
            $data  = GroupModel::withSearch(['keyword'], $input)
            ->order($input['prop'], $order)
            ->select();
            return json(['status' => 'success', 'message' => '', 'data' => $data]);
        } else {
            return View::fetch();
        }
    }

    /**
     * 保存新建的资源
     */
    public function save()
    {
        try {
            $input = input();
            validate(GroupValidate::class)->scene('save')->check($input);
            $save = GroupModel::create([
                'pid'      => $input['pid'],
                'title'    => $input['title'],
                'integral' => $input['integral'],
                'default'  => $input['default'],
                'status'   => $input['status'],
            ]);
            if ($save->default === 1) {
                GroupModel::where('id', '<>', $save->id)->update(['default'=>0]);
            }
            return json(['status' => 'success', 'message' => '新增成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }

    /**
     * 保存更新的资源
     */
    public function update()
    {
        try {
            $input = input();
            validate(GroupValidate::class)->check($input);
            $save  = GroupModel::find($input['id']);
            // 自定义修改字段
            $save->pid      = $input['pid'];
            $save->title    = $input['title'];
            $save->integral = $input['integral'];
            $save->default  = $input['default'];
            $save->status   = $input['status'];
            $save->save();
            if ($save->default === 1) {
                GroupModel::where('id', '<>', $save->id)->update(['default'=>0]);
            }
            return json(['status' => 'success', 'message' => '修改成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }
    
    /**
     * 删除指定资源
     */
    public function delete()
    {
        try {
            $input = input();
            validate(GroupValidate::class)->scene('delete')->check($input);
            GroupModel::recursiveDestroy($input['ids']);
            return json(['status' => 'success', 'message' => '删除成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }
}
