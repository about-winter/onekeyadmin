<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\View;
use think\exception\ValidateException;
use app\admin\BaseController;
use app\admin\validate\AdminGroup as GroupValidate;
use app\admin\model\AdminGroup as GroupModel;
/**
 * 管理员角色组管理
 */
class AdminGroup extends BaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        if ($this->request->isPost()) {
            $input = input();
            $order = $input['order'] === 'ascending' ? 'asc' : 'desc';
            $data  = GroupModel::withSearch(['keyword'], $input)
            ->append(['disabled'])
            ->order($input['prop'], $order)
            ->select();
            return json(['status' => 'success', 'message' => '', 'data' => $data]);
        } else {
            View::assign('menu', $this->request->menu);
            return View::fetch();
        }
    }

    /**
     * 保存新建的资源
     */
    public function save()
    {
        try {
            $input = input();
            validate(GroupValidate::class)->scene('save')->check($input);
            GroupModel::create([
                'pid'    => $input['pid'],
                'title'  => $input['title'],
                'role'   => $input['role'],
                'status' => $input['status']
            ]);
            return json(['status' => 'success', 'message' => '新增成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }

    /**
     * 保存更新的资源
     */
    public function update()
    {
        try {
            $input = input();
            validate(GroupValidate::class)->check($input);
            $save  = GroupModel::find($input['id']);
            $save->pid    = $input['pid'];
            $save->title  = $input['title'];
            $save->role   = $input['role'];
            $save->status = $input['status'];
            $save->save();
            return json(['status' => 'success', 'message' => '修改成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }

    /**
     * 删除指定资源
     */
    public function delete()
    {
        try {
            $input = input();
            validate(GroupValidate::class)->scene('delete')->check($input);
            GroupModel::recursiveDestroy($input['ids']);
            return json(['status' => 'success', 'message' => '删除成功']);
        } catch ( ValidateException $e ) {
            return json($e->getError());
        }
    }
}
