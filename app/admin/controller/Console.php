<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\View;
use think\facade\Event;
use app\admin\BaseController;
/**
 * 控制台
 */
class Console extends BaseController
{
    public function index()
    {
    	$html = event("ConsoleHtml");
        $consoleEventHeader  = "";
        $consoleEventContent = "";
        $consoleEventFooter  = "";
        foreach ($html as $key => $val) {
            $consoleEventHeader  .= isset($val["header"]) ? $val["header"] : '';
            $consoleEventContent .= isset($val["content"]) ? $val["content"] : '';
            $consoleEventFooter  .= isset($val["footer"]) ? $val["footer"] : '';;
        }
        View::assign([
            "consoleHtmlHeader"  => $consoleEventHeader,
            "consoleHtmlContent" => $consoleEventContent,
            "consoleHtmlFooter"  => $consoleEventFooter,
        ]);
        return View::fetch();
    }
}
