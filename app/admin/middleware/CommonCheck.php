<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin\middleware;
/**
 * 全局检测
 */
class CommonCheck
{
    /**
     * 不需要检查的类
     * @var noVerification
     */
    protected $ignoreCheckClass = [
        'login'
    ];

    public function handle($request, \Closure $next)
    {
        if (! in_array($request->class, $this->ignoreCheckClass)) {
            // 全局变量
            $event  = event("CommonGlobal");
            $commonGlobal = [];
            foreach ($event as $key => $val) {
                foreach ($val as $k => $v) {
                    $commonGlobal[$k] = $v;
                }
            }
            $request->commonGlobal = $commonGlobal;
            // 全局数组
            $eventArray  = event("CommonArray");
            $commonArray = [];
            foreach ($eventArray as $key => $val) {
                foreach ($val as $k => $v) {
                    $commonArray[$k][] = $v;
                }
            }
            $request->commonArray = $commonArray;
            // 全局html
            $event = event("CommonHtml");
            $commonHtmlMeta    = "";
            $commonHtmlHeader  = "";
            $commonHtmlContent = "";
            $commonHtmlFooter  = "";
            foreach ($event as $key => $val) {
                $commonHtmlMeta    .= isset($val["meta"]) ? $val["meta"] : '';
                $commonHtmlHeader  .= isset($val["header"]) ? $val["header"] : '';
                $commonHtmlContent .= isset($val["content"]) ? $val["content"] : '';
                $commonHtmlFooter  .= isset($val["footer"]) ? $val["footer"] : '';;
            }
            $request->commonHtmlMeta    = $commonHtmlMeta;
            $request->commonHtmlHeader  = $commonHtmlHeader;
            $request->commonHtmlContent = $commonHtmlContent;
            $request->commonHtmlFooter  = $commonHtmlFooter;
        }
        return $next($request);
    }
}