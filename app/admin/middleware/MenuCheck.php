<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin\middleware;

use app\admin\model\AdminMenu;
/**
 * 菜单检查
 */
class MenuCheck
{
    /**
     * 不需要检查的类
     * @var noVerification
     */
    protected $ignoreCheckClass = [
        'login'
    ];

    public function handle($request, \Closure $next)
    {
        if (! in_array($request->class, $this->ignoreCheckClass)) {
            $request->systemMenu = $this->systemMenu($request);
            $request->pluginMenu = $this->pluginMenu($request, count($request->systemMenu));
            $request->menu = array_sort(array_merge($request->systemMenu, $request->pluginMenu), 'sort');
            $publicMenu = [];
            foreach ($request->menu as $key => $value) {
                if ($value['ifshow'] === 1) {
                    array_push($publicMenu, $value);
                }
            }
            $request->publicMenu = $publicMenu;
        }
        return $next($request);
    }

    /**
     * 系统菜单
     */
    public function systemMenu($request)
    {
        $where = [];
        if ($request->userInfo['group_role'] !== "*") {
            $where[] = ['id', 'in', $request->userInfo['group_role']];
        }
        $menu = AdminMenu::where($where)->select();
        $menu = $menu ? $menu->toArray() : [];
        foreach ($menu as $key => $value) {
            $menu[$key]['unread'] = 0;
        }
        return $menu;
    }

    /**
     * 插件菜单
     */
    public function pluginMenu($request, $sort)
    {
        $pluginMenu = [];
        foreach (plugin_list() as $index => $plugin) {
            $fileMenu = plugin_path() . $plugin['name'] . '/menu.php';
            if (is_file($fileMenu)) {
                $menu = include($fileMenu);
                $menu = is_array($menu) ? $menu : [];
                $pluginMenu = $this->pluginMenuRecursion($menu, 0, $plugin['name'], $request->userInfo['group_role'],$sort);
            }
        }
        return $pluginMenu;
    }

    /**
     * 插件菜单无限级(递归)
     */
    public function pluginMenuRecursion($array, $pid, $pluginName, $role, $sort)
    {
        static $pluginMenu = [];
        foreach ($array as $key => $menu) {
            $sub['id']     = $pid === 0 ? $pluginName . '_' . $key : $pid . '_' . $key;
            if (isset($menu['bind'])) {
                $bind = AdminMenu::where('title', $menu['bind'])->value('id');
                $pid =  $bind ? $bind : 0;
            }
            $sub['pid']          = $pid;
            $sub['title']        = $menu['title'];
            $sub['path']         = $pluginName . '/' . $menu['path'];
            $sub['icon']         = isset($menu['icon']) ? $menu['icon'] : '';
            $sub['sort']         = isset($menu['sort']) ? 0 - $sort + $menu['sort'] : $sort;
            $sub['ifshow']       = isset($menu['ifshow']) ? $menu['ifshow'] : '';
            $sub['status']       = isset($menu['status']) ? $menu['status'] : '';
            $sub['unread']       = isset($menu['unread']) ? $menu['unread'] : 0;
            $sub['logwriting']   = isset($menu['logwriting']) ? $menu['logwriting'] : 0;
            $sub['operateClose'] = '插件菜单';
            $roleArr = explode(',', $role);
            if (in_array($sub['id'], $roleArr) || $role === "*") {
                $pluginMenu[]  = $sub;
            }
            if (isset($menu['children'])) {
               $this->pluginMenuRecursion($menu['children'], $sub['id'], $pluginName, $role, $sort);
            }
        }
        return $pluginMenu;
    }
}