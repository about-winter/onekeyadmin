<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin\middleware;

use app\admin\addons\Login;
/**
 * 登录检查
 */
class LoginCheck
{
    /**
     * 不需要检查的类
     * @var noVerification
     */
    protected $ignoreCheckClass = [
        'login'
    ];

    public function handle($request, \Closure $next)
    {
        Login::checkAutomaticLogin();
        $request->userInfo = session('admin_user');
        if (! in_array($request->class, $this->ignoreCheckClass)) {
            if (empty($request->userInfo)) {
                if ($request->isGet()) {
                    cookie('admin_last_url', $request->authorityPath);
                }
                return $request->isPost() ? json(['status'=>'login', 'message'=>'登录状态过期失效！']) : redirect((string)url('login/index'));
            }
        }
        return $next($request);
    }
}