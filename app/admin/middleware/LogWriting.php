<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin\middleware;

use think\facade\Log;
/**
 * 日志写入
 */
class LogWriting
{
    public function handle($request, \Closure $next)
    {
        if ($request->isPost()) {
            if (! empty($request->menu)) {
                $operation = $request->menu[$request->authorityIndex];
                // 判断是否开启日志写入
                if (isset($operation['logwriting']) && $operation['logwriting'] === 1) {
                    $parentId    = $operation['pid'];
                    $title       = $operation['title'];
                    $parentTitle = "";
                    if (! empty($parentId)) {
                        foreach ($request->menu as $key => $value) {
                            if ($parentId === $value['id']) {
                                $parentTitle = $value['title'];
                            }
                        }
                    }
                    $info = [
                        'userId'      => $request->userInfo['id'],
                        'userAccount' => $request->userInfo['account'],
                        'lang'        => $request->lang,
                        'title'       => $title . $parentTitle,
                        'path'        => $request->authorityPath,
                        'ip'          => $request->ip(),
                        'post'        => json_encode(input()),
                    ];
                    Log::channel('business')->info(serialize($info));
                }
            }
        }
        return $next($request);
    }
}