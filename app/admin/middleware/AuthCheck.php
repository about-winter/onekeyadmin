<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin\middleware;
/**
 * 权限检查
 */
class AuthCheck
{
    /**
     * 不需要检查的类
     * @var noVerification
     */
    protected $ignoreCheckClass = [
        'login'
    ];
    
    public function handle($request, \Closure $next)
    {
        if (! in_array($request->class, $this->ignoreCheckClass)) {
            $request->authorityPathList = array_column($request->menu, 'path');
            $request->authorityIndex = array_search($request->authorityPath, $request->authorityPathList);
            if ($request->path !== 'index/index') {
                if ($request->authorityIndex === false) {
                    return $request->isPost() ? json(['status' => 'error', 'message' => '当前权限不足~']) : abort(403, '当前权限不足~');
                }
            }
        }
        return $next($request);
    }
}