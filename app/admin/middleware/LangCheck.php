<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin\middleware;
/**
 * 语言检查
 */
class LangCheck
{
    public function handle($request, \Closure $next)
    {
    	$config = config('lang');
        $request->lang        = empty(input('lang')) ? $config['default_lang'] : input('lang');
        $request->langAllow   = $config['lang_allow'];
        $request->langDefault = $config['default_lang'];
        // 当前语言的所有参数（系统）
        $langParameter = [];
        if ($config['extend_list']) {
            if (isset($config['extend_list'][$request->lang])) {
                foreach ($config['extend_list'][$request->lang] as $key => $val) {
                    $parameter     = is_file($val) ? include($val) : [];
                    $langParameter = array_merge($langParameter, $parameter);
                }
            }
        }
        $request->langParameter = $langParameter;
        return $next($request);
    }
}