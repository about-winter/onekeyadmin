<?php
// +----------------------------------------------------------------------
// | 事件
// +----------------------------------------------------------------------

// 系统
$system = [
    // 系统
    'AppInit'     => [],
    'HttpRun'     => [],
    'HttpEnd'     => [],
    'LogWrite'    => [],
    'RouteLoaded' => [],
];
// 后台
$admin = [
    // 控制台html
    'ConsoleHtml'   => [],
    // 全局变量
    'CommonGlobal'  => [],
    // 全局数组
    'CommonArray'    => [],
    // 全局html
    'CommonHtml'    => [],
    // 上传结束
    'UploadEnd'     => [],
    // 系统配置
    'ConfigHook'    => [],
    // 用户配置钩子
    'UserConfigHook'=> [],
    // 后台管理员自定义字段查询结束
    'UserAdminField' => [],
];
// 公共
$common = [
    // 用户自定义字段查询结束
    'UserField' => [],
];
$plugins = plugin_list();
$pluginsPath = plugin_path();
foreach ($admin as $fileName => $value) {
    foreach ($plugins as $key => $plugin) {
        $path = $plugin['name'] . '/admin/listen/' . $fileName;
        $file = $pluginsPath . $path  . '.php';
        if (is_file($file)) {
            $listen = 'plugins\\' . str_replace('/', '\\', $path);
            array_push($admin[$fileName], $listen);
        }
    }
}
// 全局
foreach ($common as $fileName => $value) {
    foreach ($plugins as $key => $plugin) {
        $path = $plugin['name'] . '/listen/' . $fileName;
        $file = $pluginsPath . $path  . '.php';
        if (is_file($file)) {
            $listen = 'plugins\\' . str_replace('/', '\\', $path);
            array_push($common[$fileName], $listen);
        }
    }
}
$listens = array_merge($system, $admin, $common);
// 事件定义文件
$list   = [
    // 绑定事件
    'bind'      => [],
    // 监听事件
    'listen'    => $listens,
    // 订阅事件
    'subscribe' => [],
];
return $list;