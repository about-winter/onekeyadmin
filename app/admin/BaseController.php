<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin;

use think\App;
use think\facade\View;
/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;
        // 控制器初始化
        $this->initialize();
    }
    // 初始化
    protected function initialize()
    {
        // 模板变量
        $commonView = str_replace('\\', '/', app_path() . 'view\common\\');
        View::assign([
            'version'           => config('app.version'),
            'api'               => config('app.api'),
            'links'             => plugin_route('link'),
            'domain'            => $this->request->domain() . $this->request->root(),
            'header'            => $commonView . 'header.html',
            'component'         => $commonView . 'component.html',
            'footer'            => $commonView . 'footer.html',
            'lang'              => $this->request->lang,
            'langDefault'       => $this->request->langDefault,
            'langAllow'         => $this->request->langAllow,
            'langParameter'     => $this->request->langParameter,
            'userInfo'          => $this->request->userInfo,
            'authorityPath'     => $this->request->authorityPathList,
            'commonHtmlHeader'  => $this->request->commonHtmlHeader,
            'commonHtmlContent' => $this->request->commonHtmlContent,
            'commonHtmlFooter'  => $this->request->commonHtmlFooter,
            'commonGlobal'      => $this->request->commonGlobal,
            'commonArray'       => $this->request->commonArray,
        ]);
    }
}
