<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);
/**
 * 获取远程文件类型(比如：www.xxxx.com/photo)
 */
function online_filetype(string $url)
{
    $url = parse_url($url);
    if ($fp = @fsockopen($url['host'], empty($url['port']) ? 80 : $url['port'], $error)) {
        fputs($fp, "GET " . (empty($url['path']) ? '/' : $url['path']) . " HTTP/1.1\r\n");
        fputs($fp, "Host: {$url['host']}\r\n\r\n");
        while (!feof($fp)) {
            $tmp = fgets($fp);
            if (trim($tmp) == '') {
                break;
            } else if (preg_match('/Content-Type:(.*)/si', $tmp, $arr)) {
                return trim((string)$arr[1]);
            }
        }
        return null;
    } else {
        return null;
    }
}
/**
 * 引入插件common.php文件
 */
foreach (plugin_list() as $k => $v) {
    $pluginCommonFile = plugin_path() . $v['name'] . '/admin/common.php';
    if (is_file($pluginCommonFile)) {
        include($pluginCommonFile);
    }
}