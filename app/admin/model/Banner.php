<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class Banner extends Model
{
    // 设置json类型字段
    protected $json = ['link'];

    // 设置JSON数据返回数组
    protected $jsonAssoc = true;

    // 搜索器
    public function searchKeywordAttr($query, $value)
    {
    	if (! empty($value)) {
	        $query->where("content",'like', '%' . $value . '%');
	    }
    }

    public function searchLanguageAttr($query, $value)
    {
        $query->where("language", request()->lang);
    }

    // 获取器
    public function getCatalogIdAttr($value, $array)
    {
        return $value ? array_map('intval', explode(',', $value)) : [];
    }

    public function getContentAttr($value, $array)
    {
        return content_replace($value);
    }

    public function setCatalogIdAttr($value, $array)
    {
        return implode(',', $value);
    }
}