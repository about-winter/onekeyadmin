<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;
use app\admin\model\Themes;

class Config extends Model
{
    // 设置json类型字段
    protected $json = ['value'];

    // 设置JSON数据返回数组
    protected $jsonAssoc = true;
    
    // 搜索器
    public function searchLanguageAttr($query, $value, $array)
    {
        $language = request()->lang;
        $query->where("language", $language);
    }
    
    public function searchNameAttr($query, $value, $array)
    {
        if (! empty($value)) {
            $query->where("name", $value);
        }
    }

    /**
     * 配置设置
     */
    public static function setVal($name, $title, $value)
    {
        if ($name === 'basics') $name = $name . '_' . request()->lang;
        $where[] = ['name', '=', $name];
        $save = self::where($where)->find();
        if ($save) {
            $save->value = $value;
            $save->save();
        } else {
            self::create([
                'name'     => $name,
                'title'    => $title,
                'value'    => $value,
            ]);
        }
        cache($name, NULL);
    }

    /**
     * 配置获取
     */
    public static function getVal($name)
    {
        switch ($name) {
            case 'themes':
                $template  = [config('app.pc_template'),config('app.mp_template')];
                $themes    = Themes::whereIn('name', $template)->field('name,title,config')->select();
                $themesLen = count($themes);
                foreach ($themes as $index => $theme) {
                    $name   = 'theme_' . $theme['name'] . '_' . request()->lang;
                    $config = self::getVal($name);
                    $value  = $theme->config;
                    foreach ($theme['config'] as $key => $val) {
                        $valueConfig = isset($config[$val['field']]) ? $config[$val['field']] : $val['value'];
                        $value[$key]['value'] = $val['name'] === 'editor' ? content_replace($valueConfig) : $valueConfig;
                    }
                    if ($themesLen === 1) {
                        $theme->tips = '主题配置'; 
                    } else {
                        $theme->tips = config('app.pc_template') === $theme['name'] ? '主题配置-电脑' : '主题配置-手机'; 
                    }
                    $theme->config = $value;
                }
                return $themes;
                break;
            default:
                if ($name === 'basics') $name = $name . '_' . request()->lang;
                $where[] = ['name', '=', $name];
                $config  = self::where($where)->cache($name)->find();
                return $config ? $config->value : [];
                break;
        }
    }
}