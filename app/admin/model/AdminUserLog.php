<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class AdminUserLog extends Model
{
    // 关联模型
    public function user()
    {
        return $this->hasOne(AdminUser::class, 'id', 'user_id')->bind([
            'user_account' => 'account'
        ]);
    }

    // 获取器
    public function getBrowserAttr($value, $data)
    {
        return $data['useragent'];
    }

    // 搜索器
    public function searchKeywordAttr($query, $value, $array)
    {
        if (! empty($value)) {
            $query->where("title|url|ip",'like', '%' . $value . '%');
        }
    }

    public function searchDateAttr($query, $value, $array)
    {
        if (! empty($value)) { 
            $query->whereBetweenTime('create_time', $value[0], $value[1]);
        }
    }

}