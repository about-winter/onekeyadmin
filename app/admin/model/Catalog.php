<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class Catalog extends Model
{
    // 设置json类型字段
    protected $json = ['links_value','field'];

    // 设置JSON数据返回数组
    protected $jsonAssoc = true;

    // 获取器
    public function getGroupIdAttr($value, $array)
    {
        return $value ? array_map('intval', explode(',', $value)) : [];
    }

    public function getContentAttr($value, $array)
    {
        return content_replace($value);
    }

    // 修改器
    public function setGroupIdAttr($value, $array)
    {
        return implode(',', $value);
    }

    public function setSeoDescriptionAttr($value, $array)
    {
        // 描述为空则获取内容中的文字信息
        $content = strip_tags($array['content']);
        return empty($value) ? substr($content,0,255) :$value;
    }

    public function setSeoUrlAttr($value, $array)
    {
        // seo链接全部为小写
        return !empty($value) ? strtolower($value) : '';
    }

    // 搜索器
    public function searchKeywordAttr($query, $value, $array)
    {
        if (! empty($value)) {
            $query->where("title",'like', '%' . $value . '%');
        }
    }

    public function searchLanguageAttr($query, $value, $array)
    {
        $query->where("language", request()->lang);
    }

    public function searchThemeAttr($query, $value, $array)
    {
        if (! empty($value)) {
            $query->where("theme", $value);
        }
    }

    // 递归删除
    public static function recursiveDestroy($ids) {
        self::destroy($ids);
        $ids = self::whereIn('pid', $ids)->column('id');
        if ($ids) self::recursiveDestroy($ids); 
    }
}