<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\install\controller;

use PDO;
use think\facade\Db;
use think\facade\View;
use think\exception\ValidateException;
use app\install\BaseController;
/**
 * 安装页面
 */
class Index extends BaseController
{
    public function index()
    {
        if ($this->request->isPost()) {
        	$res = api_post('install/getFile', $_POST);
            if ($res['status'] === 'success') {
            	$token    = $res['token'];
	            $hostname = "localhost";
	            $adminmap = $_POST['admin_url'];
				$username = $_POST['sql_username'];
				$password = $_POST['sql_password'];
				$database = $_POST['sql_database'];
				$data = date('Y-m-d H:i:s');
				$dbc = array(
				    'hostname' => 'localhost',
				    'username' => $username,
				    'password' => $password,
				    'database' => $database,
				);
				$dsn = "mysql:dbname=$database;host=$hostname";
				try {
					// 创建数据库
				    $connect = new PDO("mysql:host=$hostname", $username, $password);
				    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				    $create = "CREATE DATABASE $database DEFAULT CHARSET utf8 COLLATE utf8_general_ci";
				    $connect->exec($create);
				    // 创建数据表
				    $db  = new PDO($dsn, $username, $password);
				    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
				    $sql = file_get_contents(root_path() . 'file.sql');
				    $db->exec($sql);
				    // 初始化数据
				    $db->exec("insert  into `mk_admin_user`(`id`,`group_id`,`nickname`,`sex`,`email`,`account`,`password`,`cover`,`login_ip`,`login_count`,`login_time`,`update_time`,`create_time`,`status`) values (1,1,'".$_POST['name']."',0,'".$_POST['email']."','".$_POST['admin_account']."','".password_hash($_POST['admin_password'], PASSWORD_BCRYPT, ['cost' => 12])."','','127.0.0.1',1,'".$data."','".$data."','".$data."',1)");
				    // 初始化文件
				    $init = file_put_contents(root_path() . '.env',"APP_DEBUG = false\nAPP_TOKEN = $token\nMAP_ADMIN = $adminmap\n\n[DATABASE]\nHOSTNAME = localhost\nDATABASE = $database\nUSERNAME = $username\nPASSWORD = $password");
				    return json(['status' => 'success', 'message' => '安装成功']);
				} catch(\PDOException $e) {
				    return json(['status' => 'error', 'message' => $e->getMessage()]);
				}
            }
        } else {
        	View::assign([
        		'admin_password' => rand_id(12),
        		'domain'         => request()->domain() . request()->root(),
        		'active'         => is_file(root_path() . '.env') ? 1 : 0,
        	]);
            return View::fetch();
        }
    }
}
