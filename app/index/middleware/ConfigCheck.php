<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\index\middleware;

use app\index\model\Config;
use app\index\addons\Login as LoginAddons;
/**
 * 配置检测
 */
class ConfigCheck
{
    public function handle($request, \Closure $next)
    {
        // 主题配置
        $request->theme = Config::getVal('theme');
        // 基础配置
        $request->basics = Config::getVal('basics');
        // 用户配置
        $request->userConfig = Config::getVal('user');
        // 自动登录
        LoginAddons::checkAutomaticLogin();
        // 用户信息
        $request->userInfo = session('index_user');
        // sdk设置
        if (! empty($request->catalog)) {
            $request->catalog = get_tdk($request->catalog);
        }
        return $next($request);
    }
}