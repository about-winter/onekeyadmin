<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\index\middleware;

use app\addons\Tree;
use app\index\model\Catalog;
use app\index\model\Banner;
use app\index\model\User;
use app\index\model\Config;
/**
 * 分类检测
 */
class CatalogCheck
{
    public function handle($request, \Closure $next)
    {
        $catalogArr = input('catalogArr');
        $index = array_search('index', array_column($catalogArr, 'seo_url'));
        $catalogIndex = $index === false ? [] : $catalogArr[$index];
        $catalogList  = array_combine(array_column($catalogArr, 'id'), $catalogArr);
        $catalogNum   = [];
        $arrNum = array_unique(array_column($catalogArr, 'num'));
        foreach ($arrNum as $key => $val) {
            foreach ($catalogArr as $k => $v) {
                if ($val === $v['num']) {
                    $catalogNum[$val][] = $v;
                }
            }
        }
        foreach ($catalogNum as $key => $val) {
            if (count($val) === 1) {
                $catalogNum[$key] = $val[0];
            }
        }
        $headerArr = [];
        $footerArr = [];
        foreach ($catalogList as $key => $val) {
            if ($request->isMobile() && $val['mobile'] === 0) continue;
            switch ($val['show']) {
                case 1:
                    array_push($headerArr, $val);
                    array_push($footerArr, $val);
                    break;
                case 2:
                    array_push($headerArr, $val);
                    break;
                case 3:
                    array_push($footerArr, $val);
                    break;
            }
        }
        $headerTree  = new Tree($headerArr);
        $footerTree  = new Tree($footerArr);
        $catalogTree = new Tree($catalogList);
        $request->catalogIndex = $catalogIndex;
        $request->catalogList  = $catalogList;
        $request->catalogNum   = $catalogNum;
        $request->headerColumn = $headerTree->leaf(0); 
        $request->footerColumn = $footerTree->leaf(0);
        // 分类信息
        $catalog = input('catalog');
        if (! empty($catalog)) {
            // 幻灯片
            $banner  = Banner::where("catalog_id", "find in set", $catalog['id'])
            ->where('status', 1)
            ->where('language', $request->lang)
            ->order('sort','desc')
            ->append(['url'])
            ->cache('banner_'.$catalog['id'])
            ->select();
            $catalog['banner'] = empty($banner) ? [] : $banner->toArray();
            // 面包屑
            $parent = $catalogTree->navi($catalog['id']);
            $crumbs = $parent;
            if ($parent[0]['id'] !== $catalogIndex['id']) {
                array_unshift($crumbs, $catalogIndex);
            }
            $catalog['crumbs'] = $crumbs;
            // 每个父级id
            $i = 0;
            foreach ($parent as $k => $v) {
                $i++;
                $key = 'class' . $i;
                $catalog[$key] = $v['id'];
            }
            $request->catalog = $catalog;
        }
        return $next($request);
    }
}