<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\index\middleware;

use app\index\addons\Url;
/**
 * 安装检测
 */
class InstallCheck
{
    public function handle($request, \Closure $next)
    {
        if (! is_file(root_path().'.env') && $request->isGet()) {
            return redirect((string)url('install/index/index'));
        } else {
            return $next($request);
        }
    }
}