<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\index\middleware;

use app\index\addons\Url;
/**
 * 环境检测
 */
class AppCheck
{
    public function handle($request, \Closure $next)
    {
        event("AppCheckInit");
        // 系统参数
        $routeName = empty(input('routeName')) ? str_replace('.html', '', $request->pathinfo()) : input('routeName');
        $request->parameter      = input('parameter'); 
        $request->parameterField = is_numeric($request->parameter) ? 'id' : 'seo_url';
        $request->routeField     = is_numeric($request->routeName) ? 'id' : 'seo_url';
        $request->lang           = empty(input('lang')) ? config('lang.default_lang') : input('lang');
        $request->routeName      = str_replace($request->lang.'/', '', $routeName);
        // 语言参数
        $langInfo      = [];
        $langAllow     = config('lang.lang_allow');
        $langParameter = [];
        foreach ($langAllow as $key => $val) {
            $langAllow[$key]['url']   = Url::getLangUrl($val);
            $langAllow[$key]['title'] = lang($val['name']);
            $langAllow[$key]['cover'] = request()->domain() . request()->root() . $val['cover'];
            if ($val['name'] === $request->lang) {
                $langInfo = $langAllow[$key];
            }
        }
        foreach (config('lang.extend_list')[$request->lang] as $key => $val) {
            $parameter = is_file($val) ? include($val) : [];
            $langParameter = array_merge($langParameter, $parameter);
        }
        $request->langInfo      = $langInfo;
        $request->langAllow     = array_sort($langAllow,'sort');
        $request->langParameter = $langParameter;
        return $next($request);
    }
}