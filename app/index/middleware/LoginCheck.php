<?php 
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\index\middleware;

use think\facade\View;
/**
 * 登录检查（系统控制器调用或插件路由绑定）
 */
class LoginCheck
{
    public function handle($request, \Closure $next)
    {
        if (empty($request->userInfo)) {
            if ($request->isGet()) {
                cookie('index_last_url', $request->url(true));
            }
            return $request->isPost() ? json(['status'=>'login', 'message'=>'登录状态过期失效！']) : redirect(get_url('user/login'));
        } else {
            // 变量
            $event = event("PersonalPublic");
            $personalPublic = [];
            foreach ($event as $key => $val) {
                foreach ($val as $k => $v) {
                    array_push($personalPublic, $v);
                }
            }
            $request->personalPublic = $personalPublic;
            View::assign('personalPublic', $personalPublic);
        }
        return $next($request);
    }
}