<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\index\model;

use think\Model;
use app\index\addons\Url;

class Catalog extends Model
{
    // 设置json类型字段
    protected $json = ['field'];

    // 设置JSON数据返回数组
    protected $jsonAssoc = true;
    
    // 获取器
    public function getContentAttr($value, $array)
    {
        return content_replace($value);
    }

    public function getCoverAttr($value, $array)
    {
        return empty($value) ? '' : request()->domain() . request()->root() . $value;
    }

    public function getGroupIdAttr($value, $array)
    {   
        return empty($value) ? [] : explode(',', $value);
    }
    
    public function getUrlAttr($value, $array)
    {
        $links = json_decode($array['links_value'] ,true);
        return $array['links_type'] === 1 ? Url::getLinkUrl($links) : Url::getCatalogUrl($array);
    }
    
    public function getFieldAttr($value, $array)
    {
        $field = [];
        foreach ($value as $k => $v) {
            $fieldValue = $v['value'];
            $field[$v['field']] = field_replace($v['name'], $fieldValue);
        }
        return $field;
    }
}