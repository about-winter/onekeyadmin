<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\index\model;

use think\Model;

class User extends Model
{
    // 设置json类型字段
    protected $json = ['field'];

    protected $jsonAssoc = true;
    
    // 关联模型
    public function group()
    {
        return $this->hasOne(UserGroup::class, 'id', 'group_id')->bind([
            'group_title' => 'title'
        ]);
    }
    
    // 修改器(create和save生效)
    public function setPasswordAttr($value, $array)
    {
    	if (! empty($value)) {
    		$password = password_hash($value, PASSWORD_BCRYPT, ['cost' => 12]);	
	        $this->set('password', $password);
    	}
    }

    public function setNicknameAttr($value, $array) 
    {
        return sensitive_word_filter($value);
    }

    public function getFieldAttr($value, $array)
    {
        $event = event("UserField");
        $field = [];
        foreach ($event as $key => $val) {
            foreach ($val as $k => $v) {
                array_push($field, $v);
            }
        }
        $fieldValue = [];
        if (! empty($field)) {
            foreach ($field as $k => $v) {
                $fieldValue[$v['field']] = isset($value[$v['field']]) ? $value[$v['field']] : $v['value'];
            }
        }
        return $fieldValue;
    }
}