<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\index\model;

use think\Model;
use app\index\addons\Url;

class Banner extends Model
{
    public function getContentAttr($value, $array)
    {
        return content_replace($value);
    }
    
    public function getCatalogIdAttr($value, $array)
    {
        return $value ? array_map('intval', explode(',', $value)) : [];
    }

    public function getCoverAttr($value, $array)
    {
        return empty($value) ? '' : request()->domain() . request()->root() . $value;
    }

    public function getVideoAttr($value, $array)
    {
        return empty($value) ? '' : request()->domain() . request()->root() . $value;
    }

    public function getUrlAttr($value, $array)
    {
        if (! empty($array['link'])) {
            $link = json_decode($array['link'], true);
            return Url::getLinkUrl($link);
        } else {
            return "";
        }
    }
}