<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\index\model;

use think\Model;
use app\index\model\Themes;
use app\index\addons\Url;

class Config extends Model
{
    // 设置json类型字段
    protected $json = ['value'];

    // 设置JSON数据返回数组
    protected $jsonAssoc = true;

    /**
     * 配置获取
     */
    public static function getVal($name, $language="")
    {
        $domain = request()->domain() . request()->root();
        switch ($name) {
            case 'theme':
                // 主题配置
                $themeName   = theme_name();
                $themeConfig = self::getVal("theme_" . $themeName . "_" . request()->lang);
                $config = Themes::where('name', $themeName)->cache('themes_' . $themeName)->field('config')->find();
                $config = $config['config'];
                $field  = [];
                foreach ($config as $k => $v) {
                    $fieldValue = isset($themeConfig[$v['field']]) ? $themeConfig[$v['field']] : $v['value'];
                    $field[$v['field']] = field_replace($v['name'], $fieldValue);
                }
                return $field;
                break;
            default:
                $thisName = $name === 'basics' ? $name . '_' . request()->lang : $name;
                $where[] = ['name', '=', $thisName];
                if (! empty($language)) {
                    $where[] = ['language', '=', $language];
                }
                $config = self::where($where)->cache($thisName)->find();
                $config =  $config ? $config->value : [];
                if ($name === 'basics') {
                    $config['logo'] = empty($config['logo']) ? '' : $domain . $config['logo'];
                    $config['ico'] = empty($config['ico']) ? '' : $domain . $config['ico'];
                }
                return $config;
                break;
        }
    }
}