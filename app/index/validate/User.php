<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\index\validate;

use think\Validate;

class User extends Validate
{
    protected $rule =   [
        'loginAccount'  => 'require',
        'loginPassword' => 'require',
        'account'  => [
            'require',
            'min'  => 5,
            'max'  => 40, 
        ],
        'email'         => 'require|email',
        'captcha'       => 'require|captcha',
        'code'          => 'require',
        'nickname'      => 'require|max:12',
        'describe'      => 'require|max:255',
        'password'      => [
            'require',
            'min'  => 6,
            'max'  => 40, 
            'regex'=> '/((?=.*[a-z])(?=.*\d)|(?=[a-z])(?=.*[#@!~%^&*])|(?=.*\d)(?=.*[#@!~%^&*]))[a-z\d#@!~%^&*]{8,16}/i'
        ],
    ];
    protected $message  =   [
        'loginAccount.require'   => ['status' => 'error', 'message' => '登录账号不能为空'],
        'loginPassword.require'  => ['status' => 'error', 'message' => '登录密码不能为空'],
        'account.require'        => ['status' => 'error', 'message' => '账号不能为空'],
        'account.min'            => ['status' => 'error', 'message' => '账号不能少于5个字符'],
        'account.max'            => ['status' => 'error', 'message' => '账号不能超过40个字符'],
        'captcha.require'        => ['status' => 'error', 'message' => '请输入验证码'],
        'captcha.captcha'        => ['status' => 'error', 'message' => '验证码错误'],
        'email.require'          => ['status' => 'error', 'message' => '邮箱号不能为空'],
        'email.email'            => ['status' => 'error', 'message' => '邮箱格式错误！'],
        'nickname.require'       => ['status' => 'error', 'message' => '昵称不能为空'],
        'nickname.max'           => ['status' => 'error', 'message' => '昵称不能超过12个字符'],
        'describe.require'       => ['status' => 'error', 'message' => '介绍不能为空'],
        'describe.max'           => ['status' => 'error', 'message' => '介绍不能超过255个字符'],
        'password.require'       => ['status' => 'error', 'message' => '密码不能为空'],
        'password.min'           => ['status' => 'error', 'message' => '密码不能少于6个字符'],
        'password.max'           => ['status' => 'error', 'message' => '密码不能超过40个字符'],
        'password.regex'         => ['status' => 'error', 'message' => '密码不能是纯数字、纯字母、纯特殊字符'],
    ];
    protected $scene = [
        'login'         => ['loginAccount','loginPassword'],
        'captchalogin'  => ['loginAccount','loginPassword','captcha'],
        'password'      => ['email','password','code'],
        'register'      => ['email','account','password','code'],
        'code'          => ['email','captcha'],
        'set'           => ['nickname','describe'],
    ];
}