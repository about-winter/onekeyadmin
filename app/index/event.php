<?php
// +----------------------------------------------------------------------
// | 事件
// +----------------------------------------------------------------------

// 系统
$system = [
    // 系统
    'AppInit'     => [],
    'HttpRun'     => [],
    'HttpEnd'     => [],
    'LogWrite'    => [],
    'RouteLoaded' => [],
];

// 前台
$index = [
    // 应用开始检测
    'AppCheckInit'  => [],
    // 全局变量
    'CommonGlobal'  => [],
    // 全局html
    'CommonHtml'    => [],
    // 个人中心
    'UserDashboard' => [],
    // 个人侧边
    'PersonalPublic'=> [],
    // 中间件执行完毕
    'RouteCheckEnd' => [],
    // 会员注册完成
    'UserRegisterEnd' => [],
    // 会员第三方登录
    'UserLoginApi' => [],
    // 会员登录前
    'UserLoginInit' => [],
];
// 公共
$common = [
    // 用户自定义字段查询结束
    'UserField' => [],
];
$plugins = plugin_list();
$pluginsPath = plugin_path();
// 前台
foreach ($index as $fileName => $value) {
    foreach ($plugins as $key => $plugin) {
        $path = $plugin['name'] . '/index/listen/' . $fileName;
        $file = $pluginsPath . $path  . '.php';
        if (is_file($file)) {
            $listen = 'plugins\\' . str_replace('/', '\\', $path);
            array_push($index[$fileName], $listen);
        }
    }
}
// 全局
foreach ($common as $fileName => $value) {
    foreach ($plugins as $key => $plugin) {
        $path = $plugin['name'] . '/listen/' . $fileName;
        $file = $pluginsPath . $path  . '.php';
        if (is_file($file)) {
            $listen = 'plugins\\' . str_replace('/', '\\', $path);
            array_push($common[$fileName], $listen);
        }
    }
}
$listens = array_merge($system, $index, $common);
// 事件定义文件
$list = [
    // 绑定事件
    'bind'      => [],
    // 监听事件
    'listen'    => $listens,
    // 订阅事件
    'subscribe' => [],
];
return $list;