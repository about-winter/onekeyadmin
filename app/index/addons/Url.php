<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\index\addons;

use think\facade\Db;
use think\facade\Route;
/**
 * 链接组件
 */
class Url
{

    /**
     * 获取url例：about
     * @param  [string]  $url  [链接]
     * @return [string]        [返回链接]
     */
    public static function getUrl($url): string
    {
        $input  = input();
        $domain = request()->domain() . request()->root();// 兼容localhost
        $suffix = empty(config('route.url_html_suffix')) ? '' : '.'. config('route.url_html_suffix');
        $lang   = isset($input['lang']) ? $input['lang'] : $_GET['lang'];
        $lang   = config('lang.default_lang') === $lang ? '' : $lang;
        $url    = $url === 'index' ||  $url === '/' ? '' : $url . $suffix;
        $url    = empty($lang) ? $domain .'/'. $url : $domain .'/'. $lang .'/'.  $url;
        return $url;
    }
    /**
     * 获取语言链接
     */
    public static function getLangUrl(array $info): string 
    {
        $domain = request()->domain() . request()->root();// 兼容localhost
        return $info['default'] === 0 ? $domain .'/'. $info['name'] : $domain;
    }

    /**
     * 获取link的url
     * @param  [array]  $link  [链接组]
     * @return [string]        [返回链接]
     */
    public static function getLinkUrl($link = []): string
    {
        $url    = "";
        $field  = 'id,seo_url';
        switch ($link['type']) {
            case '1':
                $url = $link['value'][1]['value'];
                break;
            case '2':
                $name  = "";
                $type  = "";
                $table = "";
                $id    = $link['value'][2]['value'];
                foreach (plugin_route('link') as $key => $val) {
                    if ($val['name'] === $link['value'][2]['linkName']) {
                        $name  = $val['name'];
                        $type  = $val['type'];
                        $table = $val['table'];
                        // 如果是分类表
                        if ($table === 'catalog') {
                            $catalogArr = input('catalogArr');
                            foreach ($catalogArr as $key => $value) {
                                if ($value['id'] == $id) {
                                    $url = self::getCatalogUrl($value);
                                }
                            }
                        } else {
                            // 分类类型不一定是在catalog表
                            $exist = Db::query("SHOW TABLES LIKE 'mk_".$table."'");
                            if ($exist) {
                                $details = Db::name($table)->where('id', $id)->field($field)->find();
                                if ($details) {
                                    $url = $type === 'catalog' ? self::getCatalogUrl($details) : self::getSingleUrl($details, $name);
                                }
                            }
                        }
                    }
                }
                break;
            case '3':
                $id      = $link['value'][3]['pageId'];
                $details = isset(request()->catalogList[$id]) ? request()->catalogList[$id] : ['id'=>$id];
                $url     = self::getCatalogUrl($details);
                $anchor  = $link['value'][3]['value'];
                if (! empty($anchor)) {
                    $url = $url . $anchor;
                }
                break;
        }
        return $url;
    }

    /**
     * 获取分类url(包含系统页)例：news、product
     * @param  [array] $catalog [分类详情]
     * @return [string]         [返回链接]
     */
    public static function getCatalogUrl(array $catalog): string
    {
        return empty($catalog['seo_url']) ? self::getUrl((string)$catalog['id']) : self::getUrl($catalog['seo_url']);
    }

    /**
     * 获取单页url例：news/1
     * @param  [array]   $details      [单页详情]
     * @param  [string]  $routeName    [路由名称]
     * @return [string]                [返回链接]
     */
    public static function getSingleUrl(array $details, string $routeName = ""): string
    {
        $parameter = empty($details['seo_url']) ? $details['id'] : $details['seo_url'];
        $url       = $routeName .'/'. $parameter;
        return self::getUrl($url);
    }

    /**
     * 获取分页url例：product/page/1
     * @return [string]                [返回链接]
     */
    public static function getCatalogPageUrl(int $page): string
    {
        return self::getUrl(input('routeName') .'/page/'. $page);
    }
}