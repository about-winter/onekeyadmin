<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

use think\facade\View;
use app\addons\Tree;
use app\index\addons\Url;
/**
 * 获取url
 */
function get_url(string $url): string
{
    return Url::getUrl($url);
}

/**
 * 获取子分类(树形)
 * @param  $id   当前ID
 * @return array
 */
function get_catalog_child(int $id): array
{
    $tree = new Tree(request()->catalogList);
    $catalog = $tree->leaf($id);
    return $catalog;
}

/**
 * 获取分类TDK
 */
function get_tdk($arr)
{
    $arr['seo_title'] = empty($arr['seo_title']) ? request()->basics['seo_title'] : $arr['seo_title'];
    $arr['seo_keywords'] = empty($arr['seo_keywords']) ? request()->basics['seo_keywords'] : $arr['seo_keywords'];
    $arr['seo_description'] = empty($arr['seo_description']) ? request()->basics['seo_description'] : $arr['seo_description'];
    return $arr;
}

/**
 * 设置TDK
 */
function set_tdk($obj) 
{
    $catalog = request()->catalog;
    if (isset($obj->seo_title) && ! empty($obj->seo_title)) {
        $catalog['seo_title'] = $obj->seo_title;
    }
    if (isset($obj->seo_keywords) && ! empty($obj->seo_keywords)) {
        $catalog['seo_keywords'] = $obj->seo_keywords;
    }
    if (isset($obj->seo_description) && ! empty($obj->seo_description)) {
        $catalog['seo_description'] = $obj->seo_description;
    }
    View::assign('catalog', $catalog);
}

/**
 * 替换自定义字段的链接(兼容localhost)
 * @param  string         $name 字段类型名称
 * @param  string|array   $name 字段值
 * @return string|array
 */
function field_replace(string $name, $fieldValue)
{
    $domain = request()->domain() . request()->root();
    switch ($name) {
        case 'link':
            $fieldValue = Url::getLinkUrl($fieldValue);
            break;
        case 'fileList':
        case 'imageList':
            foreach ($fieldValue as $key => $val) {
                $fieldValue[$key]['url'] = empty($val['url']) ? '' : $domain . $val['url'];
                $fieldValue[$key]['cover'] = empty($val['cover']) ? '' : $domain . $val['cover'];
            }
            break;
        case 'arrayList':
            foreach ($fieldValue as $key => $val) {
                $keys = array_keys($val);
                foreach ($keys as $i => $s) {
                    if (strstr($s, 'value_image_') || strstr($s, 'value_file_')) {
                        $fieldValue[$key][$s] = empty($val[$s]) ? '' : $domain . $val[$s];
                    }
                    if (strstr($s, 'value_link_')) {
                        $fieldValue[$key][$s] = empty($val[$s]) ? '' : Url::getLinkUrl($val[$s]);
                    }
                }
            }
            break;
        case 'editor':
            $fieldValue = content_replace($fieldValue);
            break;
        case 'imageUpload':
        case 'fileUpload':
            $fieldValue = empty($fieldValue) ? '' : $domain . $fieldValue;
            break;
    }
    return $fieldValue;
}

/**
 * 敏感词过滤
 * @param  string $str 字符串
 * @return bool
 */
function sensitive_word_filter(string $str)
{
    $list = ['操','你妈','官方','狗东西','傻逼','是狗','废物','草','逼','鸡巴'];  //定义敏感词数组
    $count = 0;
    $sensitiveWord = '';  //违规词
    $stringAfter = $str;  //替换后的内容
    $pattern = "/".implode("|",$list)."/i"; //定义正则表达式
    if(preg_match_all($pattern, $str, $matches)){ //匹配到了结果
        $patternList = $matches[0];  //匹配到的数组
        $count = count($patternList);
        $sensitiveWord = implode(',', $patternList); //敏感词数组转字符串
        $replaceArray = array_combine($patternList,array_fill(0,count($patternList),'*')); //把匹配到的数组进行合并，替换使用
        $stringAfter = strtr($str, $replaceArray); //结果替换
    }
    return $stringAfter;
}

/**
 * 引入插件common.php文件
 */
foreach (plugin_list() as $k => $v) {
    $pluginCommonFile = plugin_path() . $v['name'] . '/index/common.php';
    if (is_file($pluginCommonFile)) {
        include($pluginCommonFile);
    }
}