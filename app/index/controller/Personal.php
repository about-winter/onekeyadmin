<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\index\controller;

use think\facade\View;
use think\facade\Filesystem;
use app\index\BaseController;
use think\exception\ValidateException;
use app\index\model\UserLog;
use app\index\model\UserGroup;
use app\index\model\User as UserModel;
use app\index\validate\User as UserValidate;
/**
 * 个人中心模块
 */
class Personal  extends BaseController
{
    /**
     * 登录检测中间件
     */
    protected $middleware = [\app\index\middleware\LoginCheck::class];

    /**
     * 个人中心
     */
    public function index()
    {
        $dashboard = [];
        $dashboardEvent = event('UserDashboard');
        foreach ($dashboardEvent as $key => $value) {
            foreach ($value as $k => $v) {
                array_push($dashboard, $v);
            }
        }
        View::assign('dashboard',$dashboard);
        return View::fetch("user/index");
    }

    /**
     * 上传
     */
    public function upload()
    {
        $file     = $this->request->file('file');
        $fileSize = config('upload.size')['image'];
        $fileExt  = config('upload.ext')['image'];
        $input    = input();
        try {
            validate([ 'file' => ['fileExt' => $fileExt, 'fileSize' => $fileSize]])->check(['file' => $file]);
            $url = Filesystem::putFile('user', $file);
            $url = '/upload/' . str_replace('\\', '/', $url);
            return json(['status' => 'success', 'message' => '上传成功', 'data' => $url]);
        } catch (ValidateException $e) {
            return json(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    /**
     * 个人钱包
     */
    public function balance()
    {
        return View::fetch("user/balance");
    }

    /**
     * 我的账单
     */
    public function balanceLog()
    {
        if ($this->request->isPost()) {
            $input = input();
            $where[] = ['user_id', '=', $this->request->userInfo['id']];
            $where[] = ['type', '=', 1];
            $count = UserLog::where($where)->withSearch(['keyword','date'], $input)->count();
            $data  = UserLog::where($where)->withSearch(['keyword','date'], $input)->order('create_time', 'desc')->page($input['page'], 10)->select();
            return json(['status'=>'success','message'=>'查询成功', 'data' => $data, 'count' => $count]);
        } else {
            return View::fetch("user/integral_log");
        }
    }

    /**
     * 账户设置
     */
    public function set()
    {
        if ($this->request->isPost()) {
            try {
                $input = input();
                validate(UserValidate::class)->scene('set')->check($input);
                $this->request->userInfo->cover = $input['cover'];
                $this->request->userInfo->nickname = $input['nickname'];
                $this->request->userInfo->describe = $input['describe'];
                $this->request->userInfo->save();
                session('index_user',$this->request->userInfo);
                return json(['status'=>'success','message'=>'修改成功']);
            } catch ( ValidateException $e ) {
                return json($e->getError());
            }
        } else {
            return View::fetch("user/set");
        }
    }

    /**
     * 积分日志
     */
    public function integralLog()
    {
        if ($this->request->isPost()) {
            $input = input();
            $where[] = ['user_id', '=', $this->request->userInfo['id']];
            $where[] = ['type', '=', 0];
            $count = UserLog::where($where)->withSearch(['keyword','date'], $input)->count();
            $data  = UserLog::where($where)->withSearch(['keyword','date'], $input)->order('create_time', 'desc')->page($input['page'], 10)->select();
            return json(['status'=>'success','message'=>'查询成功', 'data' => $data, 'count' => $count]);
        } else {
            return View::fetch("user/integral_log");
        }
    }
}
