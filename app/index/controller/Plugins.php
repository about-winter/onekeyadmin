<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\index\controller;

use app\index\BaseController;
use think\facade\View;
/**
* 插件跳转
*/
class Plugins extends BaseController
{
	public function index()
    {
        $pathArr   = explode('/', input("route"));
        $name      = input("plugin"); // 插件名
        $class     = ucfirst($pathArr[0]); // 插件类
        $action    = $pathArr[1]; // 插件方法
        $route     = "plugins\\$name\index"; //插件路径
        $namespace = "$route\controller"; //命名空间
        $plugins   = app("$namespace\\$class");
        // 自动加载插件common头尾文件
        $common = [];
        foreach (['Header','Footer'] as $key => $val) {
            $commonFile = strtolower($val) . '.html'; 
            $commonPath = str_replace('\\', '/', public_path() . "$route\\view\\common\\$commonFile");
            $commonName = 'plugin' . $val;
            if (is_file($commonPath)) {
                $common[$commonName] = $commonPath;
            }
        }
        View::assign($common);
        // 执行插件
        return $plugins->$action();
    }
}