<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\index;

use think\App;
use think\facade\View;
/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {
        // 第三方登录事件绑定
        $event = event('UserLoginApi');
        $loginApi = [];
        foreach ($event as $key => $value) {
            foreach ($value as $k => $v) {
                array_push($loginApi, $v);
            }
        }
        $this->baseInfo = [
            'header'            => theme_path_now() . 'common/header.html',
            'component'         => theme_path_now() . 'common/component.html',
            'footer'            => theme_path_now() . 'common/footer.html',
            'loginApi'          => $loginApi,
            "searchCatalog"     => plugin_route('catalog'),
            'isMobile'          => $this->request->isMobile(),
            "domain"            => $this->request->domain() . $this->request->root(),
            "basics"            => $this->request->basics,
            "theme"             => $this->request->theme,
            'headerColumn'      => $this->request->headerColumn,
            'footerColumn'      => $this->request->footerColumn,
            "catalog"           => $this->request->catalog,
            "catalogNum"        => $this->request->catalogNum,
            "catalogList"       => $this->request->catalogList,
            "catalogIndex"      => $this->request->catalogIndex,
            "userInfo"          => $this->request->userInfo,
            'personalPublic'    => $this->request->personalPublic,
            "userConfig"        => $this->request->userConfig,
            "lang"              => $this->request->lang,
            'langAllow'         => $this->request->langAllow,
            'langParameter'     => $this->request->langParameter,
            "routeName"         => $this->request->routeName,
            "commonGlobal"      => $this->request->commonGlobal,
            "commonHtmlMeta"    => $this->request->commonHtmlMeta,
            "commonHtmlHeader"  => $this->request->commonHtmlHeader,
            "commonHtmlContent" => $this->request->commonHtmlContent,
            "commonHtmlFooter"  => $this->request->commonHtmlFooter,
        ];
        View::assign($this->baseInfo);
    }
}
