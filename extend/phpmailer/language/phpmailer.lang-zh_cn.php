<?php
/**
* PHPMailer language file: refer to English translation for definitive list
* Simplified Chinese Version
* @author liqwei <liqwei@liqwei.com>
*/

$PHPMAILER_LANG['authenticate']         = '邮箱SMTP 错误：登录失败。请检查账号密码是否输入正确。';
$PHPMAILER_LANG['connect_host']         = '邮箱SMTP 错误：无法连接到 SMTP 主机。请检查SMTP、端口和发送方式是否正确。';
$PHPMAILER_LANG['data_not_accepted']    = '邮箱SMTP 错误：数据不被接受。';
$PHPMAILER_LANG['empty_message']        = '邮箱内容不能为空';
$PHPMAILER_LANG['encoding']             = '邮箱未知编码: ';
$PHPMAILER_LANG['execute']              = '邮箱无法执行：';
$PHPMAILER_LANG['file_access']          = '邮箱无法访问文件：';
$PHPMAILER_LANG['file_open']            = '邮箱文件错误：无法打开文件：';
$PHPMAILER_LANG['from_failed']          = '邮箱发送地址错误：';
$PHPMAILER_LANG['instantiate']          = '邮箱未知函数调用。';
$PHPMAILER_LANG['invalid_email']        = '邮箱未发送，电子邮件地址无效：';
$PHPMAILER_LANG['mailer_not_supported'] = '邮箱发信客户端不被支持。';
$PHPMAILER_LANG['provide_address']      = '邮箱必须提供至少一个收件人地址。';
$PHPMAILER_LANG['recipients_failed']    = '邮箱SMTP 错误：收件人地址错误：';
$PHPMAILER_LANG['signing']              = '邮箱签名错误: ';
$PHPMAILER_LANG['smtp_connect_failed']  = '邮箱SMTP Connect（）失败。';
$PHPMAILER_LANG['smtp_error']           = '邮箱SMTP服务器错误: ';
$PHPMAILER_LANG['variable_set']         = '邮箱无法设置或重置变量： ';
?>